//-----------------------------LICENSE NOTICE------------------------------------
//  This file is part of CPCtelera: An Amstrad CPC Game Engine
//  Copyright (C) 2015 ronaldo / Fremos / Cheesetea / ByteRealms (@FranGallegoBR)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cpctelera.h>
#include "barra.h"
#include "bola.h"

#define VMEM (u8*)0xC000
#define calcularPosicion(x,y)  0xC000 + 80 * ((int)(y / 8)) + 2048 * (y % 8) + x


typedef struct {
	u8 x, y;
} TBarra;

typedef struct {
	u8 x, y;
} TBola;

typedef struct {
   u8 x, y, existe;
} TBloque;

const u8 g_palette[3] = {0, 2, 26};

void inicializar(){

	cpct_disableFirmware();
   	cpct_setVideoMode(0);

   	cpct_fw2hw(g_palette, 3);
   	cpct_setPalette(g_palette, 3);
}


// Borra la posicion anterior de la bola y la barra
void borrarPosicionAnterior(u8 barx, u8 bary, u8 bolx, u8 boly) {

   u8* memptr;
   u8* membola;

	memptr = cpct_getScreenPtr(VMEM, barx, bary);
	membola = cpct_getScreenPtr(VMEM, bolx, boly);
	cpct_drawSolidBox(memptr, 0, 8, 4 );
	cpct_drawSolidBox(membola, 0, 1, 4 );
}

// Escaneo de teclado para movimientos
void reconocerTeclado(u8 *barx, u8 *bolx, int *tipoMovimiento) {

	cpct_scanKeyboard_f();

	if ( cpct_isKeyPressed(Key_CursorRight) && *barx < 57 ) {
		if (*tipoMovimiento == 0)
		{
			*barx +=1;
			*bolx +=1;
		}
		else
		{
			*barx +=1;
		}
	} else if ( cpct_isKeyPressed(Key_CursorLeft) && *barx > 15 ) {
		if (*tipoMovimiento == 0)
		{
			*barx -=1;
			*bolx -=1;
		}
		else
		{
			*barx -=1;
		}	
	} else if ( cpct_isKeyPressed(Key_CursorUp) && *tipoMovimiento == 0 ) {
		*tipoMovimiento = 1;
	}
}

const TBola bol = { 40, 176};
const TBloque g_bloque1 = {16, 8, 1};
const TBloque g_bloque2 = {24, 8, 1};
const TBloque g_bloque3 = {32, 8, 1};
const TBloque g_bloque4 = {40, 8, 1};
const TBloque g_bloque5 = {48, 8, 1};
const TBloque g_bloque6 = {56, 8, 1};


// Comprueba si hay colision con alguna caja
u8 colisionCaja(TBloque* *bloques)
{
   u8 colision = 0;

   if (bol.y > 0 && bol.y < 15)
   {
      if ( (bol.x > bloques[0]->x-1 && bol.x <= bloques[0]->x+8) && (bloques[0]->existe == 1))
      {
         cpct_drawSolidBox(calcularPosicion(bloques[0]->x,bloques[0]->y),cpct_px2byteM0(0,0),7,6);
         colision = 1;
         bloques[0]->existe = 0;
      }
      if ( (bol.x > bloques[1]->x-1 && bol.x <= bloques[1]->x+8) && (bloques[1]->existe == 1))
      {
         cpct_drawSolidBox(calcularPosicion(bloques[1]->x,bloques[1]->y),cpct_px2byteM0(0,0),7,6);
         colision = 1;
         bloques[1]->existe = 0;
      }
      if ( (bol.x > bloques[2]->x-1 && bol.x <= bloques[2]->x+8) && (bloques[2]->existe == 1))
      {
         cpct_drawSolidBox(calcularPosicion(bloques[2]->x,bloques[2]->y),cpct_px2byteM0(0,0),7,6);
         colision = 1;
         bloques[2]->existe = 0;
      }
      if ( (bol.x > bloques[3]->x-1 && bol.x <= bloques[3]->x+8) && (bloques[3]->existe == 1))
      {
         cpct_drawSolidBox(calcularPosicion(bloques[3]->x,bloques[3]->y),cpct_px2byteM0(0,0),7,6);
         colision = 1;
         bloques[3]->existe = 0;
      }
      if ( (bol.x > bloques[4]->x-1 && bol.x <= bloques[4]->x+8) && (bloques[4]->existe == 1))
      {
         cpct_drawSolidBox(calcularPosicion(bloques[4]->x,bloques[4]->y),cpct_px2byteM0(0,0),7,6);
         colision = 1;
         bloques[4]->existe = 0;
      }
      if ( (bol.x > bloques[5]->x-1 && bol.x <= bloques[5]->x+8) && (bloques[5]->existe == 1))
      {
         cpct_drawSolidBox(calcularPosicion(bloques[5]->x,bloques[5]->y),cpct_px2byteM0(0,0),7,6);
         colision = 1;
         bloques[5]->existe = 0;
      }
   }


   return colision;
}

// Controla el tipo de movimiento que tiene que 
// realizar la bola. 0 parado, 1 arriba-derecha,
// 2 arriba-izquierda, 3 abajo-izquierda,
// 4 abajo-derecha
void movimientoBola(u8 barx, u8 *bolx, u8 *boly, int *tipoMovimiento, u8 *puntuacion, TBloque* *bloques) {

	if (*tipoMovimiento == 1)
	{
      if ( colisionCaja(bloques) == 1 )
      {
         *puntuacion +=1;
         *tipoMovimiento = 4;
      }
		if (*boly > 0  && *bolx < 64)
		{
			*boly -=4;
			*bolx +=1;
		}
		if (*boly == 0)
		{
			*tipoMovimiento = 4;
		}
		if (*bolx == 64)
		{
			*tipoMovimiento = 2;
		}
	}

	if (*tipoMovimiento == 2)
	{
      if ( colisionCaja(bloques) == 1 )
      {
         *puntuacion +=1;
         *tipoMovimiento = 4;
      }
		if (*boly > 0 && *bolx > 15)
		{
			*boly -=4;
			*bolx -=1;
		}
		if (*boly == 0)
		{
			*tipoMovimiento = 3;
		}
		if (*bolx == 16)
		{
			*tipoMovimiento = 1;
		}
	}

	if (*tipoMovimiento == 3)
	{
		if (*boly < 196 && *bolx > 15)
		{
			*boly +=4;
			*bolx -=1;
		}
		if (*boly == 176 & (*bolx >= barx && *bolx <= barx+4) )
		{
			*tipoMovimiento = 2;
		}
		if (*boly == 176 & (*bolx >= barx+5 && *bolx <= barx+8) )
		{
			*tipoMovimiento = 1;
		}
		if (*boly >= 190)
		{
			*tipoMovimiento = 5;
		}
		if (*bolx == 16)
		{
			*tipoMovimiento = 4;
		}
	}

	if (*tipoMovimiento == 4)
	{
		if (*boly < 196 && *bolx < 64)
		{
			*boly +=4;
			*bolx +=1;
		}
		if (*boly == 176 & (*bolx >= barx && *bolx <= barx+4) )
		{
			*tipoMovimiento = 2;
		}
		if (*boly == 176 & (*bolx >= barx+5 && *bolx <= barx+8) )
		{
			*tipoMovimiento = 1;
		}
		if (*boly >= 190)
		{
			*tipoMovimiento = 5;
		}
		if (*bolx == 64)
		{
			*tipoMovimiento = 3;
		}
	}
}



// Dibuja la bola y la barra
void dibujarNuevaPosicion(u8 barx, u8 bary, u8 bolx, u8 boly) {

   u8* memptr;
   u8* membola;

	memptr = cpct_getScreenPtr(VMEM, barx, bary);
	membola = cpct_getScreenPtr(VMEM, bolx, boly);
	cpct_drawSprite(barra, memptr, 8, 4 );
	cpct_drawSprite(bola, membola, 1, 4 );

}



/*
Como calcular esta posicion
const TBarra puntuacion = { 69, 14};
*/

const u8 puntuaciones[7] = {48,49,50,51,52,53,54};

// Dibujar la puntuacion
void dibujarPuntuacion(u8 puntuacion) {

   //u8* prueba;
   //prueba = cpct_getScreenPtr(VMEM,puntuacion.x,puntuacion.y);
   cpct_drawCharM0(0xC455,1,0,puntuaciones[puntuacion]);
}


// Desarrollo del juego
void juego() {

	int numVidas = 3;
	int tipoMovimiento = 0;
   u8 puntuacion = 0;
   u8 i = 0;

   TBloque* bloque1 = (TBloque*) g_bloque1;
   TBloque* bloque2 = (TBloque*) g_bloque2;
   TBloque* bloque3 = (TBloque*) g_bloque3;
   TBloque* bloque4 = (TBloque*) g_bloque4;
   TBloque* bloque5 = (TBloque*) g_bloque5;
   TBloque* bloque6 = (TBloque*) g_bloque6;

   TBloque* bloques[6] = { bloque1, bloque2, bloque3, bloque4, bloque5, bloque6};

   for (i=0; i<6; i++)
   {
      bloques[i]->existe = 1;
   }

	do{

		TBarra bar = { 36, 180};
      TBola* p_bol = (TBola*)bol;

      p_bol->x = 40;
      p_bol->y = 176;
      
		// Loop forever
	   	do{

	   		// Espera para borrar y dibujar
	   		cpct_waitVSYNC();

            dibujarPuntuacion(puntuacion);

	   		borrarPosicionAnterior(bar.x,bar.y,p_bol->x,p_bol->y);

	   		reconocerTeclado(&bar.x,&p_bol->x,&tipoMovimiento);

	   		movimientoBola(bar.x, &p_bol->x, &p_bol->y, &tipoMovimiento, &puntuacion, bloques);

	   		dibujarNuevaPosicion(bar.x,bar.y,p_bol->x,p_bol->y);

	   	}while (tipoMovimiento != 5 && puntuacion != 6);

 
         borrarPosicionAnterior(bar.x,bar.y,p_bol->x,p_bol->y);
   	   numVidas -=1;
   	   tipoMovimiento = 0; 

	}while(numVidas >= 1 && puntuacion != 6);
}

// Menu del juego
void menu() {

	u8* memptr;

	cpct_clearScreen(0);

	memptr = cpct_getScreenPtr(VMEM, 30, 20);
	cpct_drawStringM0("Menu",memptr, 2, 5);

	memptr = cpct_getScreenPtr(VMEM, 18, 180);
	cpct_drawStringM0("Pulsa INTRO", memptr, 2, 5);

	// Esperar respuesta del teclado
	do{
		cpct_scanKeyboard_f();
	}while(! cpct_isKeyPressed(Key_Enter) );
}

// Dibuja los margenes de la pantalla
void dibujarBordes() {

   // Borrar pantalla
   cpct_clearScreen(0);

   // Dibujar borde
   //cpct_drawSolidBox(VMEM,cpct_px2byteM0(2,2),15,200);
   //cpct_drawSolidBox(0xC041,cpct_px2byteM0(2,2),15,200);

   cpct_drawSolidBox(calcularPosicion(0,0),cpct_px2byteM0(2,2),15,200);
   cpct_drawSolidBox(calcularPosicion(65,0),cpct_px2byteM0(2,2),15,200);
}

// Dibujar los bloques
void dibujarBloques() {

   u8* prueba;
   u8 i = 0;
   
   while ( i <= 40)
   {
      //prueba = cpct_getScreenPtr(VMEM,g_bloque1.x+i,g_bloque1.y);
      cpct_drawSolidBox(calcularPosicion(i+16,8),cpct_px2byteM0(1,1),7,6);
      i = i + 8;
   }
   
}



// Programa principal
void main(void) {

	inicializar();

	while (1) {
		menu();

      dibujarBordes();

      dibujarBloques();

		juego();
	}

   	
}
