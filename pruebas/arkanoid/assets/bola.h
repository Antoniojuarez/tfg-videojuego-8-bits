// Data created with Img2CPC - (c) Retroworks - 2007-2015
#ifndef _BOLA_H_
#define _BOLA_H_

#include <types.h>

extern u8* const g_tile_tileset[1];

extern const u8 g_tile_bola[16];

#endif
