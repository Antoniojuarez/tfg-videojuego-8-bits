// Data created with Img2CPC - (c) Retroworks - 2007-2015
#ifndef _BARRA_H_
#define _BARRA_H_

#include <types.h>

extern u8* const g_tile_tileset[1];

extern const u8 g_tile_barra[32];

#endif
