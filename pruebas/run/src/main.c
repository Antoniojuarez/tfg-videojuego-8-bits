//-----------------------------LICENSE NOTICE------------------------------------
//  This file is part of CPCtelera: An Amstrad CPC Game Engine
//  Copyright (C) 2015 ronaldo / Fremos / Cheesetea / ByteRealms (@FranGallegoBR)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cpctelera.h>
#include "personaje.h"
#include "obstaculo.h"

#define VMEM (u8*)0xC000

typedef struct {
   u8 x, y;
} TPersonaje;

typedef struct {
   u8 x, y;
} TFondo;

typedef struct {
   u8 x, y;
} TObstaculo;

const u8 g_palette[4] = {0, 6, 26};

// Inicializa el modo de video y la paleta de colores
void inicializar() {

   cpct_disableFirmware();
   cpct_setVideoMode(0);

   cpct_fw2hw(g_palette, 4);
   cpct_setPalette(g_palette, 4);
}

// Muestra el menu principal del juego
void menu() {

   u8* memptr;

   cpct_clearScreen(0);

   memptr = cpct_getScreenPtr(VMEM, 30, 20);
   cpct_drawStringM0("Menu",memptr, 2, 0);

   memptr = cpct_getScreenPtr(VMEM, 18, 180);
   cpct_drawStringM0("Pulsa INTRO", memptr, 2, 0);

   // Esperar respuesta del teclado
   do{
      cpct_scanKeyboard_f();
   }while(! cpct_isKeyPressed(Key_Enter) );

}

// Borra la posicion anterior del personaje en pantalla
void borrarPosicionAnterior(u8 perx, u8 pery) {
   u8* memptr = cpct_getScreenPtr(VMEM, perx, pery);
   cpct_drawSolidBox(memptr, 0, 5, 10);
}

// Reconoce las teclas pulsadas para realizar
// el movimiento del personaje
void reconocerTeclado(u8 *perx, u8 *pery, int *salto) {

   cpct_scanKeyboard_f();

   if ( cpct_isKeyPressed(Key_CursorRight) && (*perx < 67)) {
      *perx +=1;
   }
   else if ( cpct_isKeyPressed(Key_CursorLeft) && (*perx > 8)) {
      *perx -=1;
   }
   else if ( cpct_isKeyPressed(Key_CursorUp) && *salto == 0 ) {
      *salto = 1;
   }
}

// Dibuja la nueva posicion del personaje
void dibujarNuevaPosicion(u8 perx, u8 pery) {
   u8* memptr = cpct_getScreenPtr(VMEM, perx, pery);
   cpct_drawSpriteMasked(personaje, memptr, 5, 10);
}

// Movimiento del salto
void saltar(u8 *pery, int *salto)
{
   if (*salto == 1 && *pery > 154)
   {
      *pery -= 4;
   }
   else if (*salto == 2 && *pery < 180)
   {
      *pery += 2;
   }
   else if (*pery == 152)
   {
      *salto = 2;
   }
   else if (*pery == 180)
   {
      *salto = 0;
   }
}

void dibujarObstaculo(u8 obsx, u8 obsy) {

   u8* obsptr = cpct_getScreenPtr(VMEM,obsx,obsy);
   cpct_drawSprite(obstaculo, obsptr, 5, 10);

}

void dibujarFondo(u8 fonx, u8 fony) {
   u8* fonptr = cpct_getScreenPtr(VMEM,fonx,fony);
   cpct_drawSolidBox(fonptr, cpct_px2byteM1(2,2,2,2), 64, 80);
}

void moverObstaculo(u8 *obsx) {

   if (*obsx > 0)
   {
      *obsx -=1;
   }
   if (*obsx == 1)
   {
      *obsx +=68;
   }

}

void detectarColision(u8 perx, u8 pery, u8 obsx, u8 obsy, int *colision) {

   if ( ((obsx < perx+5 && perx+5 < obsx+5) || (perx > obsx && perx < obsx+5)) && (pery+10 > obsy+5) ) {

      *colision = 1;
   }
}

const TPersonaje g_per = { 38, 180 };
const TObstaculo g_obs = { 70, 180 };
const u8 mapa[2*2] = { 1, 2, 3, 4 };

// Funcionamiento del juego
void juego() {
   TObstaculo* obs = (TObstaculo*)g_obs;
   TPersonaje* per = (TPersonaje*)g_per;
   int salto = 0;
   int colision = 0;

   obs->x = 70;
   per->x = 38;

   // Borrar pantalla del menu
   cpct_clearScreen(0);

   do{

      // Esperar para borrar
      cpct_setBorder(0);

      cpct_waitVSYNC();

      cpct_setBorder(5);

      borrarPosicionAnterior(per->x,per->y);

      borrarPosicionAnterior(obs->x,obs->y);

      reconocerTeclado(&per->x, &per->y, &salto);

      saltar(&per->y, &salto);

      moverObstaculo(&obs->x);

      detectarColision(per->x, per->y, obs->x, obs->y, &colision);

      dibujarNuevaPosicion(per->x,per->y);

      dibujarObstaculo(obs->x,obs->y);

//      cpct_setBorder(2);

//      dibujarFondo(fon->x,fon->y);

//      cpct_setBorder(3);

   }while(colision == 0);


}

// Programa principal
void main(void) {
   
   inicializar();

   while(1){
      menu();
      juego();
   }
}
