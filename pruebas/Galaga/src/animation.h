#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include <types.h>


// Posibles estados de la animación
typedef enum {
	as_null = 0,
	as_play,		// Se muestra la animacion hasta el ultimo frame
	as_cycle,		// Animacion ciclica
	as_pause,		// Animacion pausada
	as_end
} TEstadoAnimacion;

// Datos de un frame de la animacion
typedef struct {
	u8* sprite;			// Sprite asociado al frame
	u8  width, height;	// Dimensiones del sprite (en bytes)
	u8  time;			// Tiempo que el sprite se muestra
} TFrameAnimacion;

// Descripcion de la animacion como una secuencia de sprites
// Se controla por el tiempo, el cual esta medido en vueltas del bucle principal
typedef struct Animacion {
	TFrameAnimacion**     frames;	// Vector que contiene todos los frames de la animacion
	u8			 		frame_id;	// Indice del frame actual
	u8			 		time;		// Tiempo que queda de mostrar este frame
	TEstadoAnimacion	estado;		// Estado de la animacion
} TAnimacion;

extern const TFrameAnimacion g_allFramesAnimaciones[28];

extern TFrameAnimacion*	const g_Enemigo1Quieto[3];
extern TFrameAnimacion*	const g_ExplosionEnemigo[6];
extern TFrameAnimacion*	const g_ExplosionNave[5];
extern TFrameAnimacion*	const g_NaveQuieta[2];
extern TFrameAnimacion*	const g_GiroDerecha[4];
extern TFrameAnimacion*	const g_GiroIzquierda[4];
extern TFrameAnimacion*	const g_GiroDerechaCompleto[6];
extern TFrameAnimacion*	const g_GiroIzquierdaCompleto[6];
extern TFrameAnimacion*	const g_CentrarDerecha[4];
extern TFrameAnimacion*	const g_CentrarIzquierda[4];
extern TFrameAnimacion*	const g_EnemigoMovRecto[2];

extern TFrameAnimacion*	const g_Derecha[2];
extern TFrameAnimacion*	const g_Izquierda[2];

extern TFrameAnimacion*	const g_Enemigo2Quieto[3];
extern TFrameAnimacion*	const g_Abduccion[3];

extern TFrameAnimacion*	const g_CentrarDerecha2[3];
extern TFrameAnimacion*	const g_CentrarIzquierda2[3];
extern TFrameAnimacion*	const g_Enemigo2MovRecto[2];
extern TFrameAnimacion*	const g_GiroDerecha2[3];
extern TFrameAnimacion*	const g_GiroIzquierda2[3];
extern TFrameAnimacion*	const g_Derecha2[2];
extern TFrameAnimacion*	const g_Izquierda2[2];

extern TFrameAnimacion*	const g_InicioAbduccion[3];
extern TFrameAnimacion*	const g_FinAbduccion[3];

extern TFrameAnimacion*	const g_CapturaNave[5];
extern TFrameAnimacion*	const g_NaveCapturada[2];


extern TFrameAnimacion** const g_Animaciones[5][5];

i8 actualizarAnimacion(TAnimacion* anim, TFrameAnimacion** nuevaAnimacion, TEstadoAnimacion nuevoEstado);

#endif