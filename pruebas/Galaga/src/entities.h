
#ifndef _ENTITIES_H_
#define _ENTITIES_H_

#include <types.h>
#include "animation.h"

// Posibles estados de una entidad
typedef enum {
	es_parado = 0, // No se mueve
	es_movimiento, // Desplazandose por la pantalla
	es_atrapando,  // Enemigo atrapando a la nave
	es_disparando, // Disparando
	es_muerto,	   // Entidad muerta
	es_NUMEROESTADOS // Total de estados disponibles
} TEstadoEntidad;

typedef enum {
	s_izquierda = 0,
	s_derecha,
	s_arriba,
	s_abajo,
	s_nada,
	s_NUMERODIRECCIONES
} TDireccion;

typedef enum {
	t_nave = 0,
	t_enemigo1,
	t_enemigo2
} TTipo;

// Describe una entidad del juego
typedef struct Entidad {

	u8 			     *pscreen; // Puntero a la memoria de video donde la entidad es dibujada
	u8	   	        *npscreen; // Puntero a la memoria de video con la siguiente posicion donde sera dibujada
	u16					tDibu; // Controla el tiempo para que una entidad pueda ser dibujada

	u8		   	  x, nx, iniX; // Coordenadas de la entidad en pantalla (en bytes)
	u8            y, ny, iniY; // Siguientes coordenadas de la entidad en pantalla (en bytes)
	u8                   draw; // Indica si hay que redibujar la entidad

	u8                 pw, ph; // Almacena el ancho y alto de la entidad previos, para borrar una animacion
	

	TAnimacion      animacion; // Animacion actual asociada a la entidad
	TFrameAnimacion   **frame; // Puntero al siguiente frame de la animacion
	TEstadoAnimacion   estado; // Siguiente estado de la animacion

	u8					tAnim; // Controla el tiempo que tiene que pasar para cambiar la animacion de movimiento
	u8					tMovi; // Controla el tiempo que tiene que pasar hasta que un enemigo pueda volver a moverse

	TFrameAnimacion   **giroD; // Contiene el frame con el giro a la derecha del personaje
	TFrameAnimacion   **giroI; // Contiene el frame con el giro a la izquierda del personaje

	TFrameAnimacion   **girCD; // Contiene el frame con el giro desde la izquierda a derecha
	TFrameAnimacion   **girCI; // Contiene el frame con el giro desde la derecha a la izquierda

	TFrameAnimacion	  **moviC; // Mantiene el personaje en la posicion de movimiento recto
	TFrameAnimacion   **moviD; // Mantiene el personaje en la posicion movimiento derecha
	TFrameAnimacion   **moviI; // Mantiene el personaje en la posicion movimiento izquierda

	TFrameAnimacion   **centD; // Contiene el frame que sirve para centrar al personaje desde la derecha
	TFrameAnimacion   **centI; // Contiene el frame que sirve para centrar el personaje desde la izquierda

	u8			   dimX, dimY; // Dimensiones del personaje para controlar el movimiento por la pantalla

	u8					  abd; // Marca si el enemigo esta realizando la animacion de abduccion (0 - Nada, 1 - Inicio, 2 - Bucle, 3 - Fin)
							   // Para la nave sirve para marcar el tiempo que tiene que pasar hasta el siguiente disparo

	u8				   puntos; // Puntuacion obtenida al eliminar un enemigo

	

	u8					  act; // Indica si debemos actualizar una entidad

} TEntidad;

// Describe un personaje del juego
typedef struct Personaje {
	TEntidad 		   entidad; // Modelo del personaje
	TEstadoEntidad 		estado; // Estado actual del personaje
	TDireccion		 direccion; // Direccion de desplazamiento
	TTipo				  tipo; // Tipo del personaje
} TPersonaje;


TPersonaje* getPersonaje();
u16 getPuntuacion();
u16 getBajas1();
u8 getBajas2();
void realizarAccion(TPersonaje *p, TEstadoEntidad movimiento, TDireccion direccion);
u8 actualizarNave(TPersonaje *p);
void actualizarPantalla();
void dibujar();
void nuevoDisparo(TPersonaje *p);
void inicializarEntidades(u8 pantalla);
void actualizarEntidades();


#endif