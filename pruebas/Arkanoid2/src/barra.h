#include <types.h>


extern const u8 sprite_barra[32];
void dibujarBarra();
void moverBarra();
void setBarraX(u8 newX);
u8 getBarraX();
u8 getBarraY();
u8 colisionBarra();