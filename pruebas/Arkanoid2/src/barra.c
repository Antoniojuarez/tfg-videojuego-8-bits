
#include <types.h>
#include "cpctelera.h"
#include "mapa.h"
#include "constantes.h"
#include "bola.h"

typedef struct {
	u8 x, y;
}TBarra;

TBarra barra = {36,180};

// Sprite de la barra
const u8 sprite_barra[32] = {
	0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0,
	0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0,
	0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0,
	0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0
};

// Modifica la coordenada x de la barra
void setBarraX(u8 newX) {
	barra.x = newX;
}

// Recupera las coordenadas x e y de la barra
u8 getBarraX() {
	return barra.x;
}

u8 getBarraY() {
	return barra.y;
}

// Dibuja la barra en la pantalla.
void dibujarBarra() {
	cpct_drawSprite(sprite_barra,calcularPosicion(getBarraX(),getBarraY()),8,4);
}

// Inicializa las coordenadas de la barra, dibujandola.
void inicializarBarra(){
	barra.x = 36;
	barra.y = 185;
	dibujarBarra();
}

// Borra la barra de su posicion anterior.
void borrarBarra() {
	cpct_drawSolidBox(calcularPosicion(getBarraX(),getBarraY()),0,8,4);
}

// Controla el movimiento de la barra, reconoce la direccion a la que
// se va a mover, modificando la coordenada y volviendo a dibujarla.
void moverBarra() {

	borrarBarra();
	
	cpct_scanKeyboard_f();

	if ( cpct_isKeyPressed(Key_CursorRight) && getBarraX() < 60 ) {
		if (getMovimiento() == 0)
		{
			borrarBola();
			setBarraX(getBarraX() + 1);
			setX(getX()+1);
			dibujarBola();
		}
		else
		{
			setBarraX(getBarraX() + 1);
		}
	} else if ( cpct_isKeyPressed(Key_CursorLeft) && getBarraX() > 12 ) {
		if (getMovimiento() == 0)
		{
			borrarBola();
			setBarraX(getBarraX() - 1);
			setX(getX()-1);
			dibujarBola();
		}
		else
		{
			setBarraX(getBarraX() - 1);
		}	
	} else if (cpct_isKeyPressed(Key_CursorUp) && getMovimiento() == 0) {
		setMovimiento(1);
	}

	
	dibujarBarra();
}


// Comprueba la colision de la bola con la barra, modificando el tipo de
// movimiento de la bola y su velocidad, dependiendo de la zona de la barra
// donde golpee (Salida 0 -> sale hacia la izquierda, Salida 1 -> sale hacia
// la derecha)
u8 colisionBarra() {

	u8 salida = 2;

	if (getX() == getBarraX())
	{
		setDesY(2);
		salida = 0;
	}

	if (getX() >= getBarraX()+1 && getX() < getBarraX()+3)
	{
		setDesY(3);
		salida = 0;
	}

	if (getX() >= getBarraX()+3 && getX() < getBarraX()+4)
	{
		setDesY(4);
		salida = 0;
	}

	if (getX() >= getBarraX()+4 && getX() < getBarraX()+5)
	{
		setDesY(4);
		salida = 1;
	}

	if (getX() >= getBarraX()+5 && getX() < getBarraX()+7)
	{
		setDesY(3);
		salida = 1;
	}

	if (getX() == getBarraX()+7)
	{
		setDesY(2);
		salida = 1;
	}

	return salida;

}




