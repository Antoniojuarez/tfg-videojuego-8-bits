
#include <types.h>
#include "cpctelera.h"
#include "mapa.h"
#include "constantes.h"
#include "barra.h"

typedef struct {
	u8 x, y; // Coordenadas de la bola.
	u8 desX, desY; // Velocidad de desplazamiento en ambas coordenadas.
	u8 mov; // Tipo de movimiento.
}TBola;

TBola bola;


// Sprite con el dibujo de la bola
const u8 sprite_bola[16] = {
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff,
	0xff, 0xff, 0xff, 0xff
};

// Permite actualizar la coordenada x de la bola.
void setX(u8 newX) {
	bola.x = newX;
}

void setY(u8 newY) {
	bola.y = newY;
}

// Permite recuperar la coordenada x actual de la bola.
u8 getX() {
	return bola.x;
}

u8 getY() {
	return bola.y;
}

// Permite actualizar el desplazamiento de la bola
void setDesX(u8 newDesX) {
	bola.desX = newDesX;
}

void setDesY(u8 newDesY) {
	bola.desY = newDesY;
}

// Permite recuperar el desplazamiento de la bola
u8 getDesX() {
	return bola.desX;
}

u8 getDesY() {
	return bola.desY;
}

// Dibuja la bola en pantalla.
void dibujarBola(){
	cpct_drawSprite(sprite_bola,calcularPosicion(getX(),getY()),1,4);
}

// Borra la bola de la pantalla.
void borrarBola(){
	cpct_drawSolidBox(calcularPosicion(getX(),getY()),0,1,4);
}

void setMovimiento(u8 newMov) {
	bola.mov = newMov;
}

u8 getMovimiento() {
	return bola.mov;
}

// Inicializa los valores de la bola y la dibuja en su posicion inicial.
void inicializarBola(){
	bola.x = 40;
	bola.y = 180;
	bola.desX = 1;
	bola.desY = 4;
	bola.mov = 0;
	dibujarBola();
}


// Controla los 4 movimientos de la bola, comprobando si hay colision con
// los bloques de la partida, la barra o el contorno de la pantalla.
void movimientoBola(){

	borrarBola();

	// En el caso de que el movimiento de la bola sea 1
	if (getMovimiento() == 1)
	{
		// Si esta dentro de los limites de la pantalla
		// se actualizan los nuevos valores
		if (getY() > 0 && getX() < 67)
		{
			setY(getY()-getDesY());
			setX(getX()+getDesX());
		}

		// Compensacion del choque con la pared superior
		if (getDesY() == 3 && getY() == 2)
		{
			setY(0);
		}

		if (getY() == 0)
		{
			setMovimiento(4);
		}

		if (getX() == 67)
		{
			setMovimiento(2);
		}

		if (colisionBloque() == 1)
		{
			dibujarBloques(2);
			//setMovimiento(4);
		}
		
	}

	if (getMovimiento() == 2)
	{
	
		

		if (getY() > 0 && getX() > 12)
		{
			setY(getY()-getDesY());
			setX(getX()-getDesX());
		}

		// Compensacion del choque con la pared superior
		if (getDesY() == 3 && getY() == 2)
		{
			setY(0);
		}

		if (getY() == 0)
		{
			setMovimiento(3);
		}

		if (getX() == 12)
		{
			setMovimiento(1);
		}

		if (colisionBloque() == 1)
		{
			dibujarBloques(2);
			//setMovimiento(3);
		}
	}

	if (getMovimiento() == 3)
	{
		
		

		if (getY() < 196 && getX() > 12)
		{
			setY(getY()+getDesY());
			setX(getX()-getDesX());
		}

		// Compensacion del choque con la barra
		if (getDesY() == 3 && getY() == 174)
		{
			setY(176);
		}

		if (getDesY() == 2 && getY() == 173)
		{
			setY(176);
		}

		if ( getY() == 176 && colisionBarra() == 0)
		{
			setMovimiento(2);
		}

		if ( getY() == 176 && colisionBarra() == 1)
		{
			
			setMovimiento(1);
		}

		if (colisionBloque() == 1 )
		{
			dibujarBloques(2);
			//setMovimiento(1);
		}

		if (getY() >= 190)
			setMovimiento(5);

		if (getX() == 12)
			setMovimiento(4);
	}

	if (getMovimiento() == 4)
	{
		
		

		if (getY() < 196 && getX() < 67)
		{
			setY(getY()+getDesY());
			setX(getX()+getDesX());
		}

		// Compensacion del choque con la barra
		if (getDesY() == 3 && getY() == 174)
		{
			setY(176);
		}

		if (getDesY() == 2 && getY() == 173)
		{
			setY(176);
		}

		if ( getY() == 176 && colisionBarra() == 0)
		{
			
			setMovimiento(2);
		}

		if ( getY() == 176 && colisionBarra() == 1)
		{
			
			setMovimiento(1);
		}

		if (colisionBloque() == 1)
		{
			dibujarBloques(2);
			//setMovimiento(2);
		}

		if (getY() >= 190)
			setMovimiento(5);

		if (getX() == 67)
			setMovimiento(3);
	}

	dibujarBola();
}

