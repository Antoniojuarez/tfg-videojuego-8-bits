#include <types.h>

void dibujarBloques(u8 numFilas);
void dibujarBordes();
u8 getBloque(u8 pos);
void setBloque(u8 pos, u8 newValor);
u8 colisionBloque();
void inicializarMapa(u8 mapa);
u8 getPuntuacion();
void setPuntuacion(u8 newPuntuacion);
u8 getActiva();
void setActiva(u8 newAct);
