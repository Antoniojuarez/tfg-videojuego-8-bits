#include <cpctelera.h>
#include <stdio.h>
#include "constantes.h"
#include "bola.h"
#include "barra.h"
#include "random.h"

u8 bloques[16];
u8 puntuacion;
u8 activa;

const u8 puntuaciones[10] = {48,49,50,51,52,53,54,55,56,57};

u8 getActiva() {
	return activa;
}

void setActiva(u8 newAct) {
	activa = newAct;
}

// Devuelve la puntuacion de la partida.
u8 getPuntuacion() {
	return puntuacion;
}

// Actualiza la puntuacion de la partida.
void setPuntuacion(u8 newPuntuacion) {
	puntuacion = newPuntuacion;
}

// Devuelve el estado de un bloque.
u8 getBloque(u8 pos) {

	return bloques[pos];
}

// Modifica el estado de un bloque, mediante la posicion en el vector.
void setBloque(u8 pos, u8 newValor) {
	bloques[pos] = newValor;
}

// Elimina un un bloque de una fila indicada (la fila 0 es la superior).
void borrarBloque(u8 pos, u8 fila) {

	if (fila == 0)
		cpct_drawSolidBox(calcularPosicion( 12 + (7*(pos%8))  , 8 ), cpct_px2byteM0(0,0), 7, 6 );
	if (fila == 1)
		cpct_drawSolidBox(calcularPosicion( 12 + (7*(pos%8))  , 28 ), cpct_px2byteM0(0,0), 7, 6 );
}

// Dibuja la puntuacion actual en la pantalla.
void dibujarPuntuacion() {

	cpct_drawCharM0(calcularPosicion(70,5),1,0,puntuaciones[getPuntuacion()/10]);
	cpct_drawCharM0(calcularPosicion(74,5),1,0,puntuaciones[getPuntuacion()%10]);

}

// Dibuja el contorno del mapa.
void dibujarBordes() {

   cpct_clearScreen(0);

   cpct_drawSolidBox(calcularPosicion(0,0),cpct_px2byteM0(2,2),12,200);
   cpct_drawSolidBox(calcularPosicion(68,0),cpct_px2byteM0(2,2),12,200);
}


// Dibuja los bloques de la partida segun su número de filas.
void dibujarBloques(u8 numFilas) {

   u8 fila;
   u8 i = 12;
   u8 j = 8;
   u8 k = 0; 

   // Para cada fila se dibujan 8 bloques.
   for (fila = 0; fila < numFilas; fila++)
   {
   		for (k = 0; k < 8; k++)
   		{
   			if (getBloque(k+(fila*8)) != 0)
   			{
	   			cpct_drawSolidBox(calcularPosicion(i,j),cpct_px2byteM0(1,1),7,6);

	   			if (getBloque(k+(fila*8)) == 1)
	   			{
	      			cpct_drawSolidBox(calcularPosicion(i+1,j),cpct_px2byteM0(1,1),5,3);
	      		}
	      		if (getBloque(k+(fila*8)) == 2)
	      		{
	      			cpct_drawSolidBox(calcularPosicion(i+1,j),cpct_px2byteM0(4,4),5,3);	
	      		}
	      		if (getBloque(k+(fila*8)) == 3)
	      		{
	      			cpct_drawSolidBox(calcularPosicion(i+1,j),cpct_px2byteM0(3,3),5,3);	
	      		}
	      	}

      		i=i+7;
   		}
   		i = 12;
   		j = j+20;
   }
}

// Se inicializa el mapa, se asigna el estado a cada bloque,
// se dibuja tanto el contorno como los bloques y se inicializa
// la puntuacion.
void inicializarMapa(u8 mapa) {

	u8 i;
	
	if (mapa == 0)
	{
		for (i = 0; i < 16; i++)
		{
			bloques[i] = 1;
		}
	}
	if (mapa == 1)
	{
		for (i = 0; i < 16; i++)
		{
			if (i < 8)
				bloques[i] = 2;
			else
				bloques[i] = 1;
		}
	}
	if (mapa == 2)
	{
		for (i = 0; i < 16; i++)
		{
			if (i < 8)
				bloques[i] = 3;
			else
				bloques[i] = 2;
		}
	}

	dibujarBordes();
	dibujarBloques(2);

	setPuntuacion(0);
	dibujarPuntuacion();

	setActiva(1); // Permite romper bloques.

}

// Se comprueba si se ha golpeado un bloque y si
// este debe ser borrado de la pantalla.
u8 golpearBloque(u8 pos, u8 fila) {

	if (getBloque(pos) > 0)
	{
		setBloque(pos,getBloque(pos)-1);

		if (getBloque(pos) == 0)
		{
			setPuntuacion(getPuntuacion()+1);
			dibujarPuntuacion();
			borrarBloque(pos,fila);
		}

		return 1;
	}
	else
	{
		return 0;
	}
}


// Se comprueba la colisión de la bola con los bloques del juego.
u8 colisionBloque() {

	u8 colision = 0;
	u8 i = 12;
	u8 j = 0;

	if (getY() > 26 && getY() < 32)
	{
		for (j = 8; j < 16 ; j++)
		{
			if ( (getX() >= i && getX() <= i+3) && (getBloque(j) > 0) )
			{
				
				colision = golpearBloque(j,1);

				if (getMovimiento() == 1 || getMovimiento() == 2)
				{
					setMovimiento(3);
				}

				else if (getMovimiento() == 3 || getMovimiento() == 4)
				{
					setMovimiento(2);
				}
					
				return colision;
			}

			if ( (getX() >= i+3 && getX() <= i+6) && (getBloque(j) > 0) )
			{

				colision = golpearBloque(j,1);

				if (getMovimiento() == 1 || getMovimiento() == 2)
				{
					setMovimiento(4);
				}

				else if (getMovimiento() == 3 || getMovimiento() == 4)
				{
					setMovimiento(1);
				}
					
				return colision;
			}

			i=i+7;
		}
	}

	if (getY() > 10 && getY() < 13)
	{
		for (j = 0; j < 8 ; j++)
		{
			if ( (getX() >= i && getX() <= i+2) && (getBloque(j) > 0) )
			{

				colision = golpearBloque(j,0);

				if (getMovimiento() == 1 || getMovimiento() == 2)
				{
					setMovimiento(3);
				}

				else if (getMovimiento() == 3 || getMovimiento() == 4)
				{
					setMovimiento(2);
				}
				
				return colision;
			}

			if ( (getX() >= i+3 && getX() <= i+6) && (getBloque(j) > 0) )
			{

				colision = golpearBloque(j,1);

				if (getMovimiento() == 1 || getMovimiento() == 2)
				{
					setMovimiento(4);
				}

				else if (getMovimiento() == 3 || getMovimiento() == 4)
				{
					setMovimiento(1);
				}
					
				return colision;
			}

			i=i+7;
		}
	}



	return colision;
}
