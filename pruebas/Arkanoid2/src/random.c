#include <types.h>

u8 g_nextRand;

const u8 g_randUnif[3] = {1, 2, 3};

u8 getRandomUniform(u8 inc) {
	g_nextRand += inc;
	return g_randUnif[g_nextRand];
}