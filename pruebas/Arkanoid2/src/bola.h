#include <types.h>

extern const u8 sprite_bola[16];

void inicializarBola();
void setX(u8 newX);
void setY(u8 newY);
u8 getX();
u8 getY();
void setDesX(u8 newDesX);
void setDesY(u8 newDesY);
u8 getDesX();
u8 getDesY();
void dibujarBola();
void borrarBola();
void setMovimiento(u8 newMov);
u8 getMovimiento();
void movimientoBola();