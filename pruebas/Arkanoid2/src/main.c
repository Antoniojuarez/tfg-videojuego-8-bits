//-----------------------------LICENSE NOTICE------------------------------------
//  This file is part of CPCtelera: An Amstrad CPC Game Engine
//  Copyright (C) 2015 ronaldo / Fremos / Cheesetea / ByteRealms (@FranGallegoBR)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cpctelera.h>
#include "constantes.h"
#include "mapa.h"
#include "barra.h"
#include "bola.h"

const u8 g_palette[3] = {0, 2, 26, 6, 24}; // Negro, Azul, Blanco, Rojo, Amarillo

// Inicializamos el modo de video y los colores a utilizar
void inicializar() {

   cpct_disableFirmware();
   cpct_setVideoMode(0);

   cpct_fw2hw(g_palette,5);
   cpct_setPalette(g_palette,5);
}

void reiniciar() {
   borrarBarra();
   borrarBola();
   setMovimiento(0);
}

void main(void) {
   
   inicializar();

   while(1)
   {
      u8 mapa = 2;

      inicializarMapa(2);

      inicializarBarra();

      inicializarBola();

      // El movimiento 5 indica la muerte del personaje y
      // si la puntuacion es 16 hemos acabado con todos los enemigos
      while(getMovimiento() != 5 && getPuntuacion() < 16)
      {
      	// Espera a la señal de sincronización vertical del CRTC
         cpct_waitVSYNC();

         moverBarra();

         movimientoBola();

         cpct_waitVSYNC();
      }

      reiniciar();
   }
}
