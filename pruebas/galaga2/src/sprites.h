
#ifndef _SPRITES_H_
#define _SPRITES_H_


#include <types.h>

// Paletas de colores
extern const u8 Juego_palette[4];
extern const u8 Titulo_palette[4];

// Sprites
extern const u8 g_tile_Titulo[28 * 56];
extern const u8 g_tile_Nave[2 * 4 * 16];
extern const u8 g_tile_DisparoNave[2 * 1 * 8];
extern const u8 g_tile_DisparoEnemigo[2 * 1 * 8];
extern const u8 g_tile_Enemigo1_0[2 * 4 * 12];
extern const u8 g_tile_Enemigo1_1[2 * 4 * 12];
extern const u8 g_tile_ExpEnemigo1[2 * 2 * 8];
extern const u8 g_tile_ExpEnemigo2[2 * 3 * 12];
extern const u8 g_tile_ExpEnemigo3[2 * 4 * 16];
extern const u8 g_tile_ExpEnemigo4[2 * 5 * 20];
extern const u8 g_tile_ExpEnemigo5[2 * 6 * 24];
extern const u8 g_tile_ExpNave1[8 * 32];
extern const u8 g_tile_ExpNave2[8 * 32];
extern const u8 g_tile_ExpNave3[8 * 32];
extern const u8 g_tile_ExpNave4[8 * 32];
extern const u8 g_tile_Enemigo1Giro1_0[2 * 4 * 16]; // Giro a la derecha
extern const u8 g_tile_Enemigo1Giro1_1[2 * 4 * 16]; // Giro a la izquierda
extern const u8 g_tile_Enemigo1Giro2_0[2 * 4 * 16]; // Giro a la derecha
extern const u8 g_tile_Enemigo1Giro2_1[2 * 4 * 16]; // Giro a la izquierda
extern const u8 g_tile_Enemigo2_0[2 * 4 * 16];
extern const u8 g_tile_Enemigo2_1[2 * 4 * 16];

extern const u8 g_tile_Absorcion_0[2 * 8 * 48];
extern const u8 g_tile_Absorcion_1[2 * 8 * 48];

extern const u8 g_tile_Enemigo2Giro_0[2 * 4 * 16];
extern const u8 g_tile_Enemigo2Giro_1[2 * 4 * 16];

extern const u8 g_tile_IniAbsorcion_0[2 * 8 * 24];

extern const u8 g_tile_Captura_0[2 * 8 * 48];
extern const u8 g_tile_Captura_1[2 * 8 * 48];
extern const u8 g_tile_Captura_2[2 * 8 * 48];
extern const u8 g_tile_Captura_3[2 * 8 * 48];

extern const u8 g_tile_Neg[2 * 4 * 16];

extern const u8 g_tile_Nivel1[2 * 2 * 16];
extern const u8 g_tile_Nivel5[2 * 2 * 16];
extern const u8 g_tile_Nivel10[2 * 4 * 16];

#endif