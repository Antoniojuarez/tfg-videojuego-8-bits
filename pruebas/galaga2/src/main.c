//-----------------------------LICENSE NOTICE------------------------------------
//  This file is part of CPCtelera: An Amstrad CPC Game Engine
//  Copyright (C) 2015 ronaldo / Fremos / Cheesetea / ByteRealms (@FranGallegoBR)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//------------------------------------------------------------------------------

#include <cpctelera.h>
#include "inicializar.h"
#include "sprites.h"
#include "game.h"

//#include "Nave.h"
//#include "constantes.h"
//#include "Puntuaciones.h"
//#include "DisparoNave.h"
//#include "Titulo.h"
//#include "Enemigo1.h"


//const u8 g_palette[4] = { 0, 2, 6, 26}; // 0-Negro, 1-Azul, 2-Rojo, 3-Blanco


void inicializarCPC() {

  // Desactivar el firmware para que no interfiera con nuestro codigo
  cpct_disableFirmware();

  // Configurar el modo de video
  cpct_setVideoMode(1);

}

void interruptHandler() {
   static u8 i;

   i++;
   switch(i) {
      case 7: 
         cpct_scanKeyboard_if();
         break;
      case 8:
         // Play music
         cpct_akp_musicPlay();
         break;
      case 12:
         i=0;
   }
}

void main(void) {

  u16 puntuacion;
  u16 hi = 0;
  u8 jugadores;
	// Configura CPC antes de iniciar el juego
  inicializarCPC();
  
  initMusica();
   // Bucle principal que muestra el menu principal
   while (1){

    cpct_clearScreen(0);

   	jugadores = menuPrincipal(hi);

    puntuacion = game(jugadores,hi);

    // Si la puntuación de la partida finalizada es mayor
    // se actualiza la puntuacion maxima
    if (puntuacion > hi)
    {
      hi = puntuacion;
    }

   }
}
