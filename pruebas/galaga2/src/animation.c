#include <cpctelera.h>
#include "animation.h"
#include "entities.h"
#include "sprites.h"

// Vector con todos los frames que se van a utilizar
const TFrameAnimacion g_allFramesAnimaciones[28] = {
	{ g_tile_Enemigo1_0,		4, 12, 5}, // Enemigo 1 abierto
	{ g_tile_Enemigo1_1,		4, 12, 5}, // Enemigo 1 cerrado
	{ g_tile_ExpEnemigo1,   	2,  8, 1}, // ExplosionEnemigo-1
	{ g_tile_ExpEnemigo2,   	3, 12, 1}, // ExplosionEnemigo-2
	{ g_tile_ExpEnemigo3,   	4, 16, 1}, // ExplosionEnemigo-3
	{ g_tile_ExpEnemigo4,   	5, 20, 1}, // ExplosionEnemigo-4
	{ g_tile_ExpEnemigo5,   	6, 24, 1}, // ExplosionEnemigo-5
	{ g_tile_ExpNave1,      	8, 32, 8}, // ExplosionNave-1
	{ g_tile_ExpNave2,      	8, 32, 8}, // ExplosionNave-2
	{ g_tile_ExpNave3,      	8, 32, 8}, // ExplosionNave-3
	{ g_tile_ExpNave4,      	8, 32, 8}, // ExplosionNave-4
	{ g_tile_Nave,          	4, 16, 4}, // Nave
	{ g_tile_Enemigo1Giro1_0,	4, 16, 1}, // Inicio giro derecha
	{ g_tile_Enemigo1Giro2_0,	4, 16, 1}, // Fin giro derecha
	{ g_tile_Enemigo1Giro1_1,	4, 16, 1}, // Inicio giro izquierda
	{ g_tile_Enemigo1Giro2_1,	4, 16, 1}, // Fin giro izquierda
	{ g_tile_Enemigo2_0,		4, 16, 5}, // Enemigo 2 abierto
	{ g_tile_Enemigo2_1,		4, 16, 5}, // Enemigo 2 cerrado
	{ g_tile_Absorcion_0,	 	8, 48, 5},	// Absorcion pos 1
	{ g_tile_Absorcion_1,		8, 48, 5}, // Absorcion pos 2
	{ g_tile_Enemigo2Giro_0,	4, 16, 2},	// Giro a la derecha
	{ g_tile_Enemigo2Giro_1,	4, 16, 2},	// Giro a la izquierda
	{ g_tile_IniAbsorcion_0,	8, 24, 10},	// Inicio de la abduccion
	{ g_tile_Captura_0,			8, 48, 1},	// Captura-0
	{ g_tile_Captura_1,			8, 48, 1},	// Captura-1
	{ g_tile_Captura_2,			8, 48, 1},	// Captura-2
	{ g_tile_Captura_3,			8, 48, 1},	// Captura-3
	{ g_tile_Neg,		       4, 16, 2}	// Nave capturada
};

// Definimios una variable que permita acortar el nombre del vector
#define FA g_allFramesAnimaciones

// Animaciones usadas
TFrameAnimacion*	const g_Enemigo1Quieto[3] = { &FA[0], &FA[1], 0};
TFrameAnimacion*	const g_ExplosionEnemigo[6] = { &FA[2], &FA[3], &FA[4], &FA[5], &FA[6], 0};
TFrameAnimacion*	const g_ExplosionNave[5] = { &FA[7], &FA[8], &FA[9], &FA[10], 0};
TFrameAnimacion*	const g_NaveQuieta[2] = { &FA[11], 0};
TFrameAnimacion*	const g_GiroDerecha[4] = { &FA[0], &FA[12], &FA[13], 0}; // Giro a la derecha desde posicion parada
TFrameAnimacion*	const g_GiroIzquierda[4] = { &FA[0], &FA[14], &FA[15], 0}; // Giro a la izquierda desde posicion parada
TFrameAnimacion*	const g_GiroDerechaCompleto[6] = { &FA[15], &FA[14], &FA[0], &FA[12], &FA[13], 0}; // Giro a la derecha desde posicion izquierda
TFrameAnimacion*	const g_GiroIzquierdaCompleto[6] = { &FA[13], &FA[12], &FA[0], &FA[14], &FA[15], 0}; // Giro a la izquierda desde posicion derecha
TFrameAnimacion*	const g_CentrarDerecha[4] = { &FA[13], &FA[12], &FA[0], 0}; // Volvera a posicion parada desde la derecha
TFrameAnimacion*	const g_CentrarIzquierda[4] = { &FA[15], &FA[14], &FA[0], 0}; // Volvera a posicion parada desde la izquierda
TFrameAnimacion*	const g_EnemigoMovRecto[2] = { &FA[0], 0}; // Movimiento del enemigo recto
TFrameAnimacion*	const g_Derecha[2] = { &FA[13], 0};
TFrameAnimacion*	const g_Izquierda[2] = { &FA[15], 0};
TFrameAnimacion*	const g_Enemigo2Quieto[3] = { &FA[16], &FA[17], 0};
TFrameAnimacion*	const g_Abduccion[3] = { &FA[18], &FA[19], 0};

TFrameAnimacion*	const g_CentrarDerecha2[3] = { &FA[20], &FA[16], 0}; // Centrar desde la posicion derecha
TFrameAnimacion*	const g_CentrarIzquierda2[3] = { &FA[21], &FA[16], 0}; // Centrar desde la posicion izquierda
TFrameAnimacion*	const g_Enemigo2MovRecto[2] = { &FA[16], 0}; // Mantiene al enemigo recto para el movimiento
TFrameAnimacion*	const g_GiroDerecha2[3] = { &FA[16], &FA[20], 0}; // Giro del enemigo 2 a la derecha
TFrameAnimacion*	const g_GiroIzquierda2[3] = { &FA[16], &FA[21], 0}; // Giro del enemigo 2 a la izquierda
TFrameAnimacion*	const g_Derecha2[2] = { &FA[20], 0}; // Posicion derecha
TFrameAnimacion*	const g_Izquierda2[2] = { &FA[21], 0}; // Posicion izquierda

TFrameAnimacion*	const g_InicioAbduccion[3] = { &FA[22], &FA[18], 0}; // Inicio abduccion
TFrameAnimacion*	const g_FinAbduccion[3] = { &FA[18], &FA[22], 0}; // Fin de la animacion de abduccion

TFrameAnimacion*	const g_CapturaNave[5] = { &FA[23], &FA[24], &FA[25], &FA[26], 0}; // Animación de captura de la nave
TFrameAnimacion*	const g_NaveCapturada[2] = { &FA[27], 0};

// Vector con referencias a todas las animaciones
TFrameAnimacion** const g_Animaciones[5][5] = {
	{       g_NaveQuieta,   g_Enemigo1Quieto,   g_Enemigo2Quieto,                  0,       		   0},	// Estado 0 = es_parado
	{                  0,                  0,                  0,                  0,                  0},  // Estado 1 = es_movimiento
	{  g_InicioAbduccion,        g_Abduccion,     g_FinAbduccion,      g_CapturaNave,    g_NaveCapturada},  // Estado 2 = es_atrapando (Todos los estados son para el t_enemigo2)
	{                  0,                  0,                  0,                  0,                  0},  // Estado 3 = es_disparando
	{    g_ExplosionNave, g_ExplosionEnemigo, g_ExplosionEnemigo, g_ExplosionEnemigo, g_ExplosionEnemigo}	// Estado 4 = es_muerto
};
//  Tipo 0 = t_nave, Tipo 1 = t_enemigo1, Tipo 2 = t_enemigo2, Tipo 3 = t_enemigo3, Tipo 4 = t_enemigo4




i8 actualizarAnimacion(TAnimacion* anim, TFrameAnimacion** nuevaAnimacion, TEstadoAnimacion nuevoEstado) {

	i8 nuevoFrame = 0;

	// Comprobamos si tenemos una nueva animacion en la entidad
	if ( nuevaAnimacion ) {

		anim->frames = nuevaAnimacion;  			// Actualizamos la animacion de la entidad
		anim->frame_id = 0;

		if ( nuevaAnimacion[0] )
			anim->time = nuevaAnimacion[0]->time;   // Actualizamos el tiempo de la animacion

		nuevoFrame = 1;
	}

	// Si tenemos un nuevo estado lo actualizamos
	if ( nuevoEstado ) {
		anim->estado = nuevoEstado;
	}

	// Actualizamos la animacion si esta no esta parada o finalizada
	if (anim->estado != as_pause && anim->estado != as_end) {

		// Comprobamos si ha finalizado el tiempo del frame
		if ( ! --anim->time ) {

			TFrameAnimacion* frame;

			nuevoFrame = 1;
			frame = anim->frames[ ++anim->frame_id ];

			// Si el frame no es null, tenemos un nuevo frame, si no habra acabado o habra que empezarla de nuevo
			if ( frame ) {
				anim->time = frame->time;
			} else if (anim->estado == as_cycle) {
				anim->frame_id = 0;
				anim->time = anim->frames[0]->time;
			} else {
				if (anim->frames != g_ExplosionEnemigo && anim->frames != g_ExplosionNave && anim->frames != g_NaveCapturada)
				{
					--anim->frame_id;	// Hacemos que el ultimo frame sea visible
				}
				anim->estado = as_end;
			}
		}
	}

	return nuevoFrame;
}

#undef FA