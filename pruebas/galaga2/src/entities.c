
#include <stdio.h>

#include <cpctelera.h>
#include "entities.h"
#include "animation.h"
#include "sprites.h"
#include "constantes.h"



// Constantes

const u8 G_minX		= 12;
const u8 G_minY		= 10;
const u8 G_maxX		= 62;
const u8 G_maxY		= 186;

u16 g_Puntuacion;
u8 cambiaPuntuacion;

// Disparos permitidos
#define  g_MaxDisparos 2				// Numero de disparos al mismo tiempo
#define  g_MaxEnemigos 20				// Numero maximo de enemigos en pantalla
#define  g_MaxDisparosEnemigos 2		// Numero maximo de disparos de enemigos en pantalla
#define  g_MaxEnemigosMovimiento 2		// Numero maximo de enemigos en movimiento
#define  g_MaxFondo	   18				// Numero maximo de elementos mostrados en el fondo de pantalla



  TEntidad g_Disparos[g_MaxDisparos];					// Vector con los valores de los disparos
        u8 g_ultimoDisparo;				    			// Indice del vector con el ultimo disparo
TPersonaje g_Enemigos[g_MaxEnemigos];					// Vector con los enemigos
	    u8 g_ultimoEnemigo;			        			// Indice del vector con el ultimo enemigo
  TEntidad g_DisparosEnemigos[g_MaxDisparosEnemigos];   // Vector con los valores de los disparos enemigos
  		u8 g_ultimoDisparoEnemigo;						// Indice del vector con el ultimo disparo enemigo
  		u8 g_contTDibu;									// Contador de enemigos a dibujar
  		u16 g_ultimoTDibu;								// Indica el ultimo elemento existente a dibujar
  		u8 g_ultimoEnemigoMov;							// Ultimo enemigo en movimiento
  		u16	g_bajas1;									// Enemigos destruidos de tipo 1
		u8	g_bajas2; 									// Enemigos destruidos de tipo 2
  TEntidad g_FondoPantalla[g_MaxFondo];					// Contiene los elementos del fondo de pantalla
  		u8 g_ultimoFondo;								// Indice del vector de fondo de pantalla

// Personaje principal
const TPersonaje g_Personaje_init = {
	// Valores de entidad
	{
		calcularPosicion(36,170),
		calcularPosicion(36,170),
		0,
		36,36,36,
		170,170,170,
		0,
		0,0,
		{ g_NaveQuieta, 0, 4, as_cycle },
		0, as_cycle,
		0,
		0,
		0,0,
		0,0,
		0,0,0,
		0,0,
		0,0,
		0,
		0,
		0
	},
	es_parado,
	s_nada,
	t_nave
};

// Enemigo numero 1
const TPersonaje g_Enemigo1_init = {

	//Valores de entidad
	{
		calcularPosicion(14,10),
		calcularPosicion(14,10),
		0,
		14,14,14,
		10,10,10,
		0,
		0,0,
		{ g_Enemigo1Quieto, 0, 4, 2 },
		0, as_cycle,
		10,
		0,
		g_GiroDerecha,g_GiroIzquierda,
		g_GiroDerechaCompleto,g_GiroIzquierdaCompleto,
		g_EnemigoMovRecto,g_Derecha,g_Izquierda,
		g_CentrarDerecha,g_CentrarIzquierda,
		0,
		0,0,
		100,
		0
	},
	es_parado,
	s_arriba,
	t_enemigo1
};

// Enemigo numero 2
const TPersonaje g_Enemigo2_init = {

	//Valores de entidad
	{
		calcularPosicion(14,10),
		calcularPosicion(14,10),
		0,
		14,14,14,
		10,10,10,
		0,
		0,0,
		{ g_Enemigo2Quieto, 0, 4, 2 },
		0, as_cycle,
		10,
		0,
		g_GiroDerecha2,g_GiroIzquierda2,
		0,0,
		g_Enemigo2MovRecto,g_Derecha2,g_Izquierda2,
		g_CentrarDerecha2,g_CentrarIzquierda2,
		0,
		0,0,
		200,
		0
	},
	es_parado,
	s_arriba,
	t_enemigo2
};


TPersonaje g_Personaje;

TPersonaje* getPersonaje() { return &g_Personaje; }

u16 getPuntuacion() { return g_Puntuacion; }

u16 getBajas1() { return g_bajas1; }
u8 getBajas2() { return g_bajas2; }

void nuevoDisparo(TPersonaje *p);
void nuevoEnemigo(u8 x, u8 y, TTipo tipo,u8 dibu);
void generarFondo(u8 x, u8 y, u8 num);

// Inicializa las entidades de la pantalla
void inicializarEntidades(u8 pantalla) {
	g_ultimoDisparo = 0;
	g_ultimoEnemigo = 0;
	g_ultimoDisparoEnemigo = 0;
	g_contTDibu = 0;
	g_ultimoTDibu = 4;
	g_ultimoEnemigoMov = 0;
	g_ultimoFondo = 0;

	// Si la pantalla es la 1, hemos iniciado un juego por lo que la puntuacion es 0
	if (pantalla == 1)
	{
		g_Puntuacion = 0;
		g_bajas1 = 0;
		g_bajas2 = 0;
	}
	// Si es una pantalla superior la puntuacion debe continuar aumentando
	else
	{
		g_Puntuacion = getPuntuacion();	
		g_bajas1 = getBajas1();
		g_bajas2 = getBajas2();
	}
	
	cambiaPuntuacion = 0;

	{
//       const u8 nuEnemigos[40] = { 14, 1, 18, 1, 22, 1, 26, 2, 30, 2, 34, 2, 38, 3, 42, 3, 46, 3, 50, 4,  
//                                   14, 4, 18, 4, 22, 5, 26, 5, 30, 5, 34, 6, 38, 6, 42, 6, 46, 7, 50, 7 };
//          u8 n = 40 + 1;
//    
//          while(--n) {
//             
//          }
      u8 x, y = -10, t, i;
      
      t = 4;
      do {
            i = 10 + 1;
            y += 20;
            x = 14;
         while (--i) {
         	if (i == 3 || i == 8)
         		nuevoEnemigo(x, y, t_enemigo2, t / 4); 	
         	else
            	nuevoEnemigo(x, y, t_enemigo1, t / 4); 
            x += 4;
            ++t;
         }
      } while (y < 30);

//	   nuevoEnemigo(14,10,t_enemigo1,1);
//	nuevoEnemigo(18,10,t_enemigo1,1);
//	nuevoEnemigo(22,10,t_enemigo1,1);
//	nuevoEnemigo(26,10,t_enemigo1,2);
//	nuevoEnemigo(30,10,t_enemigo1,2);
//	nuevoEnemigo(34,10,t_enemigo1,2);
//	nuevoEnemigo(38,10,t_enemigo1,3);
//	nuevoEnemigo(42,10,t_enemigo1,3);
//	nuevoEnemigo(46,10,t_enemigo1,3);
//	nuevoEnemigo(50,10,t_enemigo1,4);
//
//	nuevoEnemigo(14,30,t_enemigo1,4);
//	nuevoEnemigo(18,30,t_enemigo1,4);
//	nuevoEnemigo(22,30,t_enemigo1,5);
//	nuevoEnemigo(26,30,t_enemigo1,5);
//	nuevoEnemigo(30,30,t_enemigo1,5);
//	nuevoEnemigo(34,30,t_enemigo1,6);
//	nuevoEnemigo(38,30,t_enemigo1,6);
//	nuevoEnemigo(42,30,t_enemigo1,6);
//	nuevoEnemigo(46,30,t_enemigo1,7);
//	nuevoEnemigo(50,30,t_enemigo2,7);
   }

	//generarFondo(14,20,3);
	//generarFondo(23,80,3);
	
	//nuevoEnemigo(50,10,t_enemigo2);
	
	//nuevoEnemigo(26,10,t_enemigo2);

	//g_ultimoTDibu = 0;

	cpct_memcpy(&g_Personaje, &g_Personaje_init, sizeof(TPersonaje));
}

/*// Genera el fondo de la pantalla
void generarFondo(u8 x, u8 y, u8 num)
{
	TEntidad *newFondo = 0;
	u8 cont;

	for (cont=0;cont<num;cont++)
	{

		newFondo = &g_FondoPantalla[g_ultimoFondo];

		newFondo->pscreen  = calcularPosicion(x,y+cont); 
		newFondo->npscreen = calcularPosicion(x,y+cont);
		newFondo->x 	   = x;
		newFondo->y        = y+cont;
		newFondo->nx	   = x;
		newFondo->ny	   = y+cont;

		newFondo->draw 	   = 1;

		newFondo->iniX 	   = x;
		newFondo->iniY	   = y+cont;

		newFondo->act      = 0;

		++g_ultimoFondo;
	}

}*/

// Actualiza las entidades en pantalla despues de una muerte del jugador
void actualizarEntidades() {

	u8 cont;
	TPersonaje *n = g_Enemigos;

	u8 iniX;
	u8 iniY;
	u8 *nuevaPos;

	cont = g_ultimoEnemigo + 1;

	while(--cont)
	{

		iniX = n->entidad.iniX;
		iniY = n->entidad.iniY;
		nuevaPos = calcularPosicion(iniX,iniY);
		
		n->entidad.x        = iniX;
		n->entidad.y        = iniY;
		n->entidad.nx       = iniX;
		n->entidad.ny       = iniY;
		n->entidad.pscreen  = nuevaPos;
		n->entidad.npscreen = nuevaPos;

		n++;
	}

	// Se recorren los enemigos que quedan vivos
	/*for (cont = 0; cont < g_ultimoEnemigo; cont++)
	{
		// Se recuperan del vector y se actualizan sus posiciones a las posiciones iniciales
		n = &g_Enemigos[cont];

		iniX = n->entidad.iniX;
		iniY = n->entidad.iniY;
		nuevaPos = calcularPosicion(n->entidad.iniX,n->entidad.iniY);
		
		n->entidad.x        = iniX;
		n->entidad.y        = iniY;
		n->entidad.nx       = iniX;
		n->entidad.ny       = iniY;
		n->entidad.pscreen  = nuevaPos;
		n->entidad.npscreen = nuevaPos;
	}*/

	cpct_memcpy(&g_Personaje, &g_Personaje_init, sizeof(TPersonaje));	
}

// Realiza los cambios en el personaje para el siguiente frame
// dependiendo de la accion que se haya realizado
void realizarAccion(TPersonaje *p, TEstadoEntidad movimiento, TDireccion direccion) {

	TEntidad *e = &p->entidad; // Se recupera la entidad asociada al personaje


	// Se comprueba la accion dependiendo del movimiento solicitado
	switch(movimiento){

		// Accion de mover la nave
		case es_movimiento:

			// Si la nave estaba parada, la ponemos en movimiento
			// indicando la direccion hacia donde se movera
			if (p->estado == es_parado || p->estado == es_disparando || p->estado == es_movimiento) {
				p->direccion = direccion;
				p->estado = es_movimiento;
			}
			break;

		// Accion de detener la nave
		case es_parado:

			// Si la nave estaba en movimiento, indicamos que se
			// debe de detener
			if (p->estado == es_movimiento || p->estado == es_disparando) {
				p->direccion = direccion;
				p->estado = es_parado;
			}
			break;

		// Accion de disparar
		case es_disparando:

			// Intentamos generar un nuevo disparo
			if (p->estado == es_movimiento || p->estado == es_parado)
			{
				p->direccion = direccion;
				p->estado = es_disparando;
			}
			break;

		default:
		;
	}	
	
}

// Genera un nuevo disparo indicando su posicion en pantalla
void nuevoDisparo(TPersonaje *p) {

	TEntidad *nave = &p->entidad;
	TEntidad *newDisparo = 0;

	u8 newX = nave->x+2;
	u8 newY = nave->y-10;
	//u8 *nuevaPosicion = calcularPosicion(nave->x+2,(nave->y-10))
	u8 *nuevaPosicion = calcularPosicion(newX,newY);

	if (p->tipo == t_nave)
	{
		if (g_ultimoDisparo < g_MaxDisparos) {

			// Asociamos los valores del nuevo disparo a su posicion en el vector
			newDisparo = &g_Disparos[g_ultimoDisparo];
			newDisparo->pscreen  = nuevaPosicion; 
			newDisparo->npscreen = nuevaPosicion;
			newDisparo->x 		 = newX;
			newDisparo->y        = newY;
			newDisparo->nx		 = newX;
			newDisparo->ny		 = newY;
			newDisparo->draw 	 =  1;

			newDisparo->tDibu    = 8;

			++g_ultimoDisparo;
		}
	}
	else if (p->tipo == t_enemigo1)
	{
		if (g_ultimoDisparoEnemigo < g_MaxDisparosEnemigos) {

			// Asociamos los valores del nuevo disparo a su posicion en el vector
			newDisparo = &g_DisparosEnemigos[g_ultimoDisparoEnemigo];
			newDisparo->pscreen  = nuevaPosicion;
			newDisparo->npscreen = nuevaPosicion;
			newDisparo->x        = newX;
			newDisparo->y        = newY;
			newDisparo->nx       = newX;
			newDisparo->ny       = newY;
			newDisparo->draw     = 1;

			++g_ultimoDisparoEnemigo;
		}
	}

}

// Genera un nuevo enemigo en el juego
void nuevoEnemigo(u8 x, u8 y, TTipo tipo,u8 dibu) {

	TPersonaje *n;
	u8 *nuevaPosicion = calcularPosicion(x,y);

	u16* pent16 = (u16*)&g_Enemigos[g_ultimoEnemigo]; // Apunta a la primera posicion de la entidad
	u8* pent8;										  // Puntero de 8 bits para los elementos de la entidad de este
/*
	if (g_ultimoEnemigo < g_MaxEnemigos) {

		if (tipo == t_enemigo1)
		{
			cpct_memcpy(pent16, &g_Enemigo1_init, sizeof(TPersonaje));
		}
		else if (tipo == t_enemigo2)
		{
			cpct_memcpy(pent16, &g_Enemigo2_init, sizeof(TPersonaje));
		}

				  *pent16 = (u16)nuevaPosicion; // pscreen
		++pent16; *pent16 = (u16)nuevaPosicion; // npscreen
		++pent16; *pent16 = dibu;				// tDibu
		++pent16;   pent8 = (u8*)pent16;
				   *pent8 = x;					// x
		++pent8;   *pent8 = x;					// nx
		++pent8;   *pent8 = x;					// iniX
		++pent8;   *pent8 = y;					// y
		++pent8;   *pent8 = y;					// ny
		++pent8;   *pent8 = y;				    // iniY
		++pent8;   *pent8 = 1;					// draw
		++g_ultimoEnemigo;
		++g_contTDibu;
	}*/


	if (g_ultimoEnemigo < g_MaxEnemigos) {

		if (tipo == t_enemigo1)
		{

			cpct_memcpy(pent16, &g_Enemigo1_init, sizeof(TPersonaje));
				*pent16 = (u16)nuevaPosicion; // pscreen
			++pent16; *pent16 = (u16)nuevaPosicion; // npscreen
			++pent16; *pent16 = dibu;				// tDibu
			++pent16;   pent8 = (u8*)pent16;
					   *pent8 = x;					// x
			++pent8;   *pent8 = x;					// nx
			++pent8;   *pent8 = x;					// iniX
			++pent8;   *pent8 = y;					// y
			++pent8;   *pent8 = y;					// ny
			++pent8;   *pent8 = y;				    // iniY
			++pent8;   *pent8 = 1;					// draw
			++g_ultimoEnemigo;
			++g_contTDibu;

			//TPersonaje *n;

			// Permite utilizar el enemigo de ejemplo para generar uno nuevo, UTILIZAR EL CODIGO
			// DE ABAJO PARA DEMOSTRAR LA DIFERENCIA EN ESAMBLADOR.
			/*cpct_memcpy(&g_Enemigos[g_ultimoEnemigo], &g_Enemigo1_init, sizeof(TPersonaje));

			n = &g_Enemigos[g_ultimoEnemigo];

			n->entidad.x 		 = x;
			n->entidad.y 		 = y;
			n->entidad.nx		 = x;
			n->entidad.ny 		 = y;
			n->entidad.pscreen	 = nuevaPosicion;
			n->entidad.npscreen  = nuevaPosicion;
			n->entidad.draw      = 1;

			n->entidad.iniX		 = x;
			n->entidad.iniY		 = y;

			++g_ultimoEnemigo;

			n->entidad.tDibu     = dibu;

			++g_contTDibu;*/

			/*if (g_contTDibu == 5 && g_ultimoEnemigo != 20)
			{
				g_ultimoTDibu = g_ultimoTDibu + 1; // Aumentamos el contador para la siguiente entidad
				g_contTDibu = 0; // Reiniciamos el contador	
			}

			
			
			TPersonaje *n;

			// Asociamos los valores del nuevo enemigo a su posicion en el vector
			n = &g_Enemigos[g_ultimoEnemigo];
			
			
			n->entidad.x 		 = x;
			n->entidad.y 		 = y;
			n->entidad.nx		 = x;
			n->entidad.ny 		 = y;
			n->entidad.pscreen	 = calcularPosicion(x,y);
			n->entidad.npscreen  = calcularPosicion(x,y);
			n->entidad.draw 	 = 0;

			n->entidad.animacion.frames = g_Enemigo1Quieto;
			n->entidad.animacion.frame_id = 0;
			n->entidad.animacion.time = 4;
			n->entidad.animacion.estado = 2;
			
			n->entidad.frame = 0;
			n->entidad.estado = as_cycle;

			n->entidad.tAnim = 10;
			n->entidad.tMovi = 0;

			n->estado = es_parado;
			n->direccion = s_arriba;
			n->tipo = t_enemigo1;
			
			++g_ultimoEnemigo;
*/
		}

		else if (tipo == t_enemigo2)
		{

			//TPersonaje *n;

			cpct_memcpy(&g_Enemigos[g_ultimoEnemigo], &g_Enemigo2_init, sizeof(TPersonaje));

			n = &g_Enemigos[g_ultimoEnemigo];

			n->entidad.x 		= x;
			n->entidad.y 		= y;
			n->entidad.nx		= x;
			n->entidad.ny 		= y;
			n->entidad.pscreen  = nuevaPosicion;
			n->entidad.npscreen = nuevaPosicion;

			n->entidad.iniX		 = x;
			n->entidad.iniY		 = y;

			++g_ultimoEnemigo;

			n->entidad.tDibu     = dibu;
			

			++g_contTDibu;
		}
		
	}

}

// Elimina la bala
void borrarBala(u8 i) {

	i8 existe = g_ultimoDisparo - i - 1;

	// Comprobamos si existe una posicion a la derecha de la que vamos
	// a borrar, para reordenar el vector
	if (existe) {
		cpct_memcpy(&g_Disparos[i],&g_Disparos[i+1],existe*sizeof(TEntidad));
	}

	// Movemos el indice del vector
	--g_ultimoDisparo;
}

// Elimina una bala enemiga
void borrarBalaEnemiga(u8 i) {

	i8 existe = g_ultimoDisparoEnemigo - i - 1;

	if (existe) {
		cpct_memcpy(&g_DisparosEnemigos[i],&g_DisparosEnemigos[i+1],existe*sizeof(TEntidad));
	}

	--g_ultimoDisparoEnemigo;
}

// Elimina un enemigo
void borrarEnemigo(u8 i) {

	i8 existe = g_ultimoEnemigo - i - 1;

	if (existe) {
		cpct_memcpy(&g_Enemigos[i],&g_Enemigos[i+1],existe*sizeof(TPersonaje));
	}

	--g_ultimoEnemigo;
}

// Mueve una bala por la pantalla
void moverBala(u8 i) {

	TEntidad *bala = &g_Disparos[i]; // Recupera la bala del array

	if (bala->y > G_minY)
	{
		bala->y        = bala->ny;
		bala->ny	   = (bala->ny-8);
		bala->pscreen  = bala->npscreen;
		bala->npscreen = calcularPosicion(bala->nx,bala->ny);
		bala->draw 	   = 1;	
	}
	else {
		borrarBala(i);
	}
}

// Mueve una bala enemiga por la pantalla
void moverBalaEnemiga(u8 i) {

	TEntidad *bala = &g_DisparosEnemigos[i];

	if (bala->y < G_maxY)
	{
		bala->y        = bala->ny;
		bala->ny       = bala->ny+8;
		bala->pscreen  = bala->npscreen;
		bala->npscreen = calcularPosicion(bala->nx,bala->ny);
		bala->draw     = 1;
	}
	else {
		borrarBalaEnemiga(i);
	}
}


// Mueve un enemigo por la pantalla
void moverEnemigo(TEntidad *e) {

	//TPersonaje *enemigo = &g_Enemigos[i]; 					// Recuperamos el enemigo
	//TEntidad         *e = &enemigo->entidad;					// Recuperamos la entidad

	u8 limI = limiteInferior(e);
	u8 limDe = limiteDerecha(e);
	u8 limIz = limiteIzquierda(e);

	u8 desInf = e->y+8;
	u8 desDe = e->x+2;
	u8 desIz = e->x-2;

	TFrameAnimacion **f = e->frame;

	// Si el enemigo se mueve recto o ha sido centrado
	if ( f == e->moviC || f == e->centD || f == e->centI )
	{

		// Desplazamiento vertical 
		if (limI < G_maxY)
		{
			e->ny = desInf;
			//e->npscreen = calcularPosicion(e->nx,e->ny);
		}

		// En caso de alcanzar el limite pasamos al borde superior de la pantalla
		else
		{
			e->ny = 10;
			//e->npscreen = calcularPosicion(e->nx,e->ny);
		}
	}

	// Si el enemigo estaba en posicion derecha o con un giro completo a la derecha
	else if ( f == e->moviD || f == e->girCD )
	{

		if (limDe <= G_maxX)
		{
			// Desplazamiento en diagonal si no alcanza ningun limite
			if (limI < G_maxY)
			{
				e->nx = desDe;
				e->ny = desInf;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}

			// Si alcanza el limite inferior, pasara al borde superior y se desplazara a la derecha
			else
			{
				e->nx = desDe;
				e->ny = 10;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}
		}
		
		else
		{
			// Si alcanza el extremo derecho, pasara a la primera posicion izquierda y se desplazara en vertical
			if (limI < G_maxY)
			{
				e->nx = 12;
				e->ny = desInf;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}

			// En caso de encontrarse en la esquina inferior derecha, pasara a la esquina superior izquierda
			else
			{
				e->nx = 12;
				e->ny = 10;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}	
		}
		
	}

	// Si el enemigo estaba en posicion izquierda o con un giro completo a la izquierda
	else if ( f == e->moviI || f == e->girCI )
	{

		if (limIz >= G_minX)
		{
			// Desplazamiento en diagonal si no alcanza ningun limite
			if (limI < G_maxY)
			{
				e->nx = desIz;
				e->ny = desInf;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}

			// Si alcanza el limite inferior, pasara al borde superior y se desplazara a la izquierda
			else
			{
				e->nx = desIz;
				e->ny = 10;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}
		}
		
		else 
		{
			// Si alcanza el extremo izquierdo, pasara a la ultima posicion de la derecha y se desplazara en vertical
			if (limI < G_maxY)
			{
				e->nx = 56;
				e->ny = desInf;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}

			// En caso de encontrarse en la esquina inferior izquierda, pasara a la esquina superior derecha
			else
			{
				e->nx = 56;
				e->ny = 10;
				//e->npscreen = calcularPosicion(e->nx,e->ny);
			}
		}
		
	}

	e->npscreen = calcularPosicion(e->nx,e->ny);
}


// Comprueba si un enemigo a colisionado
u8 comprobarColisiones(TPersonaje *p) {

	u8 colision = 0;
	u8 cont;

	TEntidad *e;

	u8 maxX = p->entidad.x+3;	// Limite de la entidad
	u8 y = p->entidad.y;

	// Comprobacion para los enemigos
	if (p->tipo != t_nave)
	{

		// Comprueba la colision con cada uno de los disparos
		for (cont = 0; cont < g_ultimoDisparo; cont++) {

			TEntidad* disparo = &g_Disparos[cont]; // Recuperamos el disparo

			if ( p->entidad.x <= disparo->x && maxX >= disparo->x)
			{
				if ( y <= disparo->y && y+12 >= disparo->y)
				{
					colision = 1;
					disparo->y = 190;
					borrarBala(cont);
					// Añadir un valor a la entidad para controlar las colisiones de las balas
				}
			}
			
		}	
	}

	// Comprobacion para la nave
	else
	{

		e = g_DisparosEnemigos;

		cont = g_ultimoDisparoEnemigo + 1;

		while(--cont)
		{

			if ( p->entidad.x <= e->x && maxX >= e->x)
			{
				if ( y <= e->y+12 )
				{
					colision = 1;
				}
			}

			e++;
		}

		
		// Comprueba si algun enemigo ha colisionado con el
		for (cont = 0; cont < g_ultimoEnemigo && colision == 0; cont++) {

			TPersonaje* enemigo = &g_Enemigos[cont];
			TEntidad *e = &enemigo->entidad;

			if (enemigo->tipo == t_enemigo2 && e->abd > 0)
			{
				if (p->entidad.x <= e->x && maxX >= e->x)
				{
					e->frame = g_Animaciones[2][3];
					e->estado = as_play;
					colision = 2;
				}
			}

			if ( p->entidad.x <= e->x && maxX >= e->x)
			{
				if ( y <= e->y+12)
				{
					colision = 1;
				}
			}
		}
	}

	

	return colision;
}

// Comprueba si la posicion para detenerse del enemigo esta desocupada
u8 detenerEnemigo(TPersonaje *p) {

	u8 detener;
	u8 cont;

	// Comprueba si el enemigo esta en la posicion inicial de salida
	if (p->entidad.y == 10)
	{
		detener = 1;


		// Se recorren los enemigos y se comprueba si algun otro enemigo que este en la posicion
		// actual, en ese caso el enemigo actual no se podra detener.
		for (cont = 0; cont < g_ultimoEnemigo; cont++)
		{

			TPersonaje* enemigo = &g_Enemigos[cont];

			if (p->entidad.x < enemigo->entidad.x && p->entidad.x+3 >= enemigo->entidad.x)
			{
				detener = 0;
			}
			else if (p->entidad.x > enemigo->entidad.x && p->entidad.x <= enemigo->entidad.x+3)
			{
				detener = 0;
			}
		}	
	}

	// Se comprueba si se ha alcanzado la posicion del enemigo 2 para la animacion de abduccion
	else if (p->entidad.y > 130 && p->entidad.y < 140 && p->tipo == t_enemigo2)
	{
		detener = 2;
	}
	else
	{
		detener = 0;
	}
	
	return detener;
}

// Lleva el control de la animacion de la abduccion por parte del t_enemigo2
void controlAbduccion(TPersonaje *enemigo) {

	TEntidad *e = &enemigo->entidad;

	// En el primer estado se inicia el efecto
	if (e->abd == 1)
	{
		e->frame = g_Animaciones[2][0];
		e->tMovi = 15;	// Indica la duracion de esta parte de la animacion
		e->estado = as_play;
		e->abd = 2;
		enemigo->estado == es_atrapando;
	}
	else if (e->tMovi == 0)
	{
		// Cuando acaba la anterior animacion se desencadena la animacion principal
		if (e->abd == 2)
		{
			e->frame = g_Animaciones[2][1];
			e->estado = as_cycle;
			e->tMovi = 30; // Duracion de esta parte de la animacion
			e->abd = 3;
		}
		// Finalizamos la animacion
		else if (e->abd == 3)
		{
			e->frame = g_Animaciones[2][2];
			e->estado = as_play;
			e->tMovi = 15;
			e->abd = 4;
		}
		// Devolvemos el enemigo a su estado anterior para que pueda volver a moverse
		else if (e->abd == 4)
		{
			e->frame = e->moviC;
			enemigo->estado == es_movimiento;
		}
	}
	
}

// Maneja el cambio de animacion según la direccion a la que se va a desplazar el enemigo
void controlMovimiento(TAnimacion *anim,TEntidad *e,TPersonaje *enemigo,u8 mov) {

	u8 play = 0;
	TFrameAnimacion **f;

	if (anim->estado != as_play)
	{
		// Caso en que el enemigo se desplazará recto
		if (mov%5 == 0)
		{
			// Si el frame actual es un giro de derecha o izquierda, se centrara el enemigo
			if (e->frame == e->giroD)
			{
				f = e->centD;
				//e->estado = as_play;
				play = 1;
			}
			else if (e->frame == e->giroI)
			{
				f = e->centI;
				//e->estado = as_play;
				play = 1;
			}

			// En caso contrario se mantendra recto
			else 
			{
				f = e->moviC;
				//e->estado = as_cycle;
			}

		}

		else {

			// Caso en que el enemigo realizara un giro a la derecha
			if (mov%2 == 0)
			{
				// Si el enemigo estaba recto se realizara el giro
				if (e->frame == e->moviC)
				{
					f = e->giroD;
					//e->estado = as_play;
					play = 1;
				}

				// Si el enemigo estaba girado a la izquierda su movimiento depende del tipo de enemigo
				else if (e->frame == e->giroI)
				{

					// El enemigo uno cambia su posicion completamente a la izquierda
					if (enemigo->tipo == t_enemigo1)
					{
						f = e->girCD;	
					}

					// El enemigo dos se centra
					else if (enemigo->tipo == t_enemigo2)
					{
						f = e->centI;
					}
					
					//e->estado = as_play;
					play = 1;
				}

				// Si ya estaba a la derecha, se mantiene girado
				else if (e->frame != e->giroD)
				{
					f = e->moviD;
					//e->estado = as_cycle;
				}

			}

			// Caso en que el enemigo realizara un giro a la izquierda
			else
			{
				// Si el enemigo estaba recto se realizara el giro
				if (e->frame == e->moviC)
				{
					f = e->giroI;
					//e->estado = as_play;
					play = 1;
				}

				// Si el enemigo estaba girado a la derecha su movimiento depende del tipo de enemigo
				else if (e->frame == e->giroD)
				{

					// El enemigo uno cambia su posicion completamente a la derecha
					if (enemigo->tipo == t_enemigo1)
					{
						f = e->girCI;
					}

					// El enemigo dos se centra
					else if (enemigo->tipo == t_enemigo2)
					{
						f = e->centD;
					}
					
					//e->estado = as_play;
					play = 1;
				}

				// Si ya estaba a la izquierda, se mantiene girado
				else if (e->frame != e->giroI)
				{
					f = e->moviI;
					//e->estado = as_cycle;
				}

			}
		}
	}
	

	e->frame = f;

	if (play)
		e->estado = as_play;
	else
		e->estado = as_cycle;
}

// Actualiza el estado de un enemigo
void actualizarEnemigo(u8 i, u8 colision) {

	TPersonaje *enemigo = &g_Enemigos[i]; 					// Recuperamos el enemigo
	TEntidad         *e = &enemigo->entidad;				// Recuperamos la entidad

	TAnimacion    *anim = &e->animacion;  					// Recuperamos la animacion
	TFrameAnimacion *fa = anim->frames[anim->frame_id];		// Obtenemos el frame actual

	u8 mov;
	u8 detener;
	

	// Se comprueba si la animacion ha terminado
	if ( fa ) { 

		// Antes de actualizar, almacenamos la posicion actual
		e->x       = e->nx;
		e->y       = e->ny;
		e->pscreen = e->npscreen;
		e->pw      = fa->width;
		e->ph      = fa->height;

			// Actualizamos la animacion con una nueva animacion o un nuevo estado
			if ( actualizarAnimacion(&e->animacion, e->frame, e->estado) ) {
				e->draw = 1;
				fa = anim->frames[anim->frame_id];
				e->frame = 0;
			}
			else {
				e->draw = 0;
			}

	} else {

		// En caso de que la animacion haya terminado comprobamos si el enemigo esta muerto
		if ( enemigo->estado == es_muerto)
		{
			e->draw = 0;
			borrarEnemigo(i);
		}
	}
	
	// Comprobamos si el enemigo esta muerto
	if ( enemigo->estado != es_muerto) {

		// comprobarColisiones(enemigo)
		// Comprobamos si al enemigo le ha impactado un misil
		if ( colision ) {
			e->frame = g_Animaciones[4][1]; //ExplosionEnemigo
			e->estado = as_play;

			if (enemigo->estado == es_movimiento)
			{
				--g_ultimoEnemigoMov;
			}

			enemigo->estado = es_muerto;
			g_Puntuacion = g_Puntuacion + e->puntos;
			cambiaPuntuacion = 1;

			cpct_akp_SFXPlay (9, 11, 72, 0, 0, AY_CHANNEL_B);
		}

		// Si no se ha producido colision, pasamos a realizar el resto de comprobaciones
		if (enemigo->estado != es_muerto)
		{

			detener = detenerEnemigo(enemigo); // Se comprueba fuera para obtener la posicion en la que se detiene el enemigo

			// Se comprueba si el enemigo se puede detener al volver a su posicion original
			if ( detener>0 && enemigo->estado == es_movimiento && e->abd == 0)
			{
				// Si el enemigo es de tipo 1 y se ha detenido quiere decir que esta en la posicion inicial
				if (enemigo->tipo == t_enemigo1)
				{
					e->frame = g_Animaciones[0][1];	// Enemigo 1 quieto
					e->tMovi = 200; // Asignamos un tiempo minimo de detencion
					enemigo->estado = es_parado;
					e->estado = as_cycle;

					if (g_ultimoEnemigoMov > 0)
					{
						--g_ultimoEnemigoMov; // Reducimos en 1 el numero de enemigos moviendose	
					}

				}
				// Si el enemigo es del tipo 2, puede estar en posicion inicial o en la de abduccion (detener == 2)
				else if (enemigo->tipo == t_enemigo2)
				{
					if (detener == 2)
					{
						e->abd = 1; // Indica que hemos alcanzado el estado de abduccion
					}
					else
					{
						e->frame = g_Animaciones[0][2];
						e->tMovi = 200; // Asignamos un tiempo minimo de detencion
						enemigo->estado = es_parado;
						e->estado = as_cycle;

						if (g_ultimoEnemigoMov > 0)
						{
							--g_ultimoEnemigoMov; // Reducimos en 1 el numero de enemigos moviendose	
						}
					}
				}
				
			}

			// Comprobacion de si el efecto de la abduccion se debe de activar
			if ( e->abd > 0 )
			{
				controlAbduccion(enemigo);
			}

			// Comprueba si el enemigo ha estado el tiempo minimo parado
			if (e->tMovi == 0)
			{
				// Comprobacion para comenzar el movimiento
				if (  cpct_rand8()%7 == 0 || enemigo->estado == es_movimiento )
				{
					
					if (g_ultimoEnemigoMov < g_MaxEnemigosMovimiento || enemigo->estado == es_movimiento)
					{

						// Se comprueba si el tiempo de la animacion ha acabado para
						// realizar el siguiente movimiento
						if (e->tAnim == 0 || e->abd == 4)
						{


							// Comprobamos si el enemigo ha comenzado el movimiento en este momento
							// para aumentar el contador de enemigos en movimiento
							if (enemigo->estado == es_parado)
							{
								g_ultimoEnemigoMov++;
							}

							enemigo->estado = es_movimiento;
							
							mov = cpct_rand8();

							controlMovimiento(anim,e,enemigo,mov);

							e->tAnim = 20;

							//moverEnemigo(i);
							moverEnemigo(e);

							e->draw = 1;

							e->abd = 0; // Al mover el enemigo finaliza la animacion de abduccion y puede volver a detenerse
						}
						else
						{
							--e->tAnim;
						}
					}

				}
			}
			else
			{
				--e->tMovi;
			}	

		}

		// Se obtiene un numero al azar para comprobar si el enemigo debe disparar
		if (enemigo->estado == es_parado && enemigo->tipo == t_enemigo1)
		{
			if (cpct_rand8()<2) {
				nuevoDisparo(enemigo);
				cpct_akp_SFXPlay (6, 11, 72, 0, 0, AY_CHANNEL_B);
			}	
		}
		

	}

	

}


/*
// Actualizacion del fondo del juego
void moverFondo(TEntidad *e) {

	if (e->y+6 < 170)
	{
		e->y = e->ny;
		e->ny = e->y+16;
		e->pscreen = e->npscreen;
		e->npscreen = calcularPosicion(e->nx,e->ny);
	}
	else
	{
		e->y = e->ny;
		e->ny = e->iniY;
		e->pscreen = e->npscreen;
		e->npscreen = calcularPosicion(e->iniX,e->iniY);
	}
}*/



// Actualiza la posicion de los elementos de la pantalla
void actualizarPantalla() {
	u8 i;
	u8 colision;
	u8 cont;

	//TPersonaje *enemigo = g_Enemigos;
	TPersonaje *enemigo;
	//TEntidad *disparo;
	//TEntidad *fondo;


		// Mover el fondo (Actualizar solo cuando toque)
		/*for (i=0; i< g_ultimoFondo; ++i) {
			
			fondo = &g_FondoPantalla[i];

			if (g_ultimoTDibu == 7)
   			{
				moverFondo(fondo);
			}
			
		}*/

	
		// Recorre el array de disparos
		for (i=0; i < g_ultimoDisparo; ++i) {
			moverBala(i);
		}
	


		// Recorre el array de disparos enemigos
		for (i=0; i < g_ultimoDisparoEnemigo; ++i) {
			moverBalaEnemiga(i);			
		}

		/*
		cont = g_ultimoEnemigo + 1;

		while(--cont)
		{

			colision = comprobarColisiones(enemigo);

			if ( colision && enemigo->tipo == t_enemigo1)
			{
				g_bajas1++;
			}
			else if ( colision && enemigo->tipo == t_enemigo2)
			{
				g_bajas2++;
			}

			if (enemigo->entidad.act == 0 || colision)
			{
				actualizarEnemigo(enemigo,colision);
				enemigo->entidad.act = 1;
			}

			enemigo++
		}*/

		
		for (i=0; i < g_ultimoEnemigo; ++i) {

			enemigo = &g_Enemigos[i]; 					// Recuperamos el enemigo

			colision = comprobarColisiones(enemigo);

			if ( colision )
			{
				if (enemigo->tipo == t_enemigo1)
				{
					g_bajas1++;
				}
				else
				{
					g_bajas2++;
				}
			}

			if (enemigo->entidad.act == 0 || colision)
			{
				actualizarEnemigo(i,colision);
				enemigo->entidad.act = 1;	
			}
			
		}
	
}

// Actualiza la nave, con sus nuevas posiciones
u8 actualizarNave(TPersonaje *p) {

	u8 colision;

	TEntidad *e = &p->entidad;

	TAnimacion    *anim = &e->animacion;  					// Recuperamos la animacion
	TFrameAnimacion *fa = anim->frames[anim->frame_id];		// Obtenemos el frame actual
	
	u8 vivo = 1;

	//if (g_ultimoTDibu == 0)
	//{
		if (g_ultimoEnemigo == 0)
		{
			vivo = 2;
		}

		// Se comprueba si la animación actual ha finalizado
		if ( fa )
		{
			// En caso que no haya finalizado se almacenan los valores actuales
			// y se actualiza al siguiente frame

			e->pscreen = e->npscreen;
			e->x 	   = e->nx;
			e->y	   = e->ny;
			e->pw	   = fa->width;
			e->ph	   = fa->height;

			if ( actualizarAnimacion(&e->animacion, e->frame, e->estado) ) {
				e->draw = 1;
				fa = anim->frames[anim->frame_id];
				e->frame = 0;
			}
			else {
				e->draw = 0;
			}

		}
		else {
			
			// Si ha finalizado la animacion dejamos de dibujar
			// e indicamos que el presonaje esta muerto
			if ( p->estado == es_muerto) {

				e->draw = 0;
				vivo = 0;
				p->estado = es_parado;
				
			}

		}

		// Si la nave sigue viva se comprueban 
		if ( vivo == 1 ) {

			colision = comprobarColisiones(p);

			if ( colision == 1 ) {
				e->frame = g_Animaciones[4][0]; // ExplosionNave
				e->estado = as_play;
				p->estado = es_muerto;
				cpct_akp_SFXPlay (9, 11, 72, 0, 0, AY_CHANNEL_B);
			}

			else if ( colision == 2) {
				e->frame = g_Animaciones[2][4];
				e->estado = as_play;
				p->estado = es_muerto;
			}

			else {
				// Comprobamos si la nave se esta moviendo
				if (p->estado == es_movimiento) {

					if (p->direccion == s_derecha && e->nx+4 < G_maxX)
					{
						e->nx = e->nx + 1;
						e->npscreen = calcularPosicion(e->nx,e->ny);
						e->draw = 1;
					}

					else if (p->direccion == s_izquierda && e->nx > G_minX)
					{
						e->nx = e->nx - 1;
						e->npscreen = calcularPosicion(e->nx,e->ny);
						e->draw = 1;
					}
				}

				// Si la nave esta parada dejamos de dibujar
				else if (p->estado == es_parado) {
					e->draw = 0;
				}

				// Comprobamos si la nave esta disparando
				else if (p->estado == es_disparando) {

					if (p->entidad.abd == 0)
					{
						nuevoDisparo(p);
						cpct_akp_SFXPlay (6, 11, 72, 0, 0, AY_CHANNEL_B);
						p->estado = es_parado;
						p->entidad.abd = 5;	
					}
					else
					{
						--p->entidad.abd;
					}
					
				}
			}
		}

	//}

	
	return vivo;
}


// Dibuja la nueva posicion de la nave
void dibujarNave(TEntidad* e) {

	if (e->draw)
	{

		TAnimacion* 	  anim = &e->animacion;
		TFrameAnimacion* frame = anim->frames[anim->frame_id];

		if ( frame )
		{
			cpct_drawSolidBox(e->pscreen,0,4,16);
			cpct_drawSpriteMasked(frame->sprite,e->npscreen,frame->width,frame->height);
		}
		else {
			cpct_drawSolidBox(e->pscreen,0,e->pw,e->ph);
		}

			
	}
}


// Dibuja un disparo en pantalla
/*void dibujarDisparo(TEntidad* e) {

	if (e->draw)
	{
		if (e->y < G_minY) {
			cpct_drawSolidBox(e->pscreen,0,1,8);
		}
		else {
			cpct_drawSolidBox(e->pscreen,0,1,8);
			cpct_drawSpriteMasked(g_tile_DisparoNave,e->npscreen,1,8);
		}
	}
}

// Dibujar un disparo enemigo en pantalla
void dibujarDisparoEnemigo(TEntidad* e) {

	if (e->draw)
	{
		if (e->y > G_maxY) {
			cpct_drawSolidBox(e->pscreen,0,1,8);
		}
		else {
			cpct_drawSolidBox(e->pscreen,0,1,8);
			cpct_drawSpriteMasked(g_tile_DisparoEnemigo,e->npscreen,1,8);
		}
	}
}*/


// Version 1
/*void dibujarDisparos(TEntidad* e, u8 tipo) {

	if (e->draw)
	{
		if ( tipo )
		{
			if (e->y < G_minY) {
				cpct_drawSolidBox(e->pscreen,0,1,8);
			}
			else {
				cpct_drawSolidBox(e->pscreen,0,1,8);
				cpct_drawSpriteMasked(g_tile_DisparoNave,e->npscreen,1,8);
			}
		}
		else
		{
			if (e->y > G_maxY) {
				cpct_drawSolidBox(e->pscreen,0,1,8);
			}
			else {
				cpct_drawSolidBox(e->pscreen,0,1,8);
				cpct_drawSpriteMasked(g_tile_DisparoEnemigo,e->npscreen,1,8);
			}
		}
	}
}*/

// Version 2
/*void dibujarDisparos(TEntidad* e, u8 tipo) {

	if (e->draw)
	{
		if (e->y < G_minY || e->y > G_maxY) {

			cpct_drawSolidBox(e->pscreen,0,1,8);
		}
		else {
			cpct_drawSolidBox(e->pscreen,0,1,8);
			if ( tipo )
				cpct_drawSpriteMasked(g_tile_DisparoNave,e->npscreen,1,8);
			else
				cpct_drawSpriteMasked(g_tile_DisparoEnemigo,e->npscreen,1,8);
		}
	}
}*/

// Version 3
void dibujarDisparos(TEntidad* e, u8 tipo) {

	if (e->draw)
	{
		cpct_drawSolidBox(e->pscreen,0,1,8);

		if (e->y > G_minY && e->y < G_maxY) {

			if ( tipo )
				cpct_drawSpriteMasked(g_tile_DisparoNave,e->npscreen,1,8);
			else
				cpct_drawSpriteMasked(g_tile_DisparoEnemigo,e->npscreen,1,8);

		}
	}
}

// Version 4
/*void dibujarDisparos(TEntidad* e, u8 tipo) {

	u8 y 	= e->y;
	u8 *ps 	= e->pscreen;
	u8 *nps = e->npscreen;

	if (e->draw)
	{
		cpct_drawSolidBox(ps,0,1,8);

		if (y > G_minY && y < G_maxY) {

			if ( tipo )
				cpct_drawSpriteMasked(g_tile_DisparoNave,nps,1,8);
			else
				cpct_drawSpriteMasked(g_tile_DisparoEnemigo,nps,1,8);

		}
	}
}*/


// Dibuja los enemigos en pantalla
void dibujarEnemigos(TEntidad* e) {

	if (e->draw)
	{
		TAnimacion* 	  anim = &e->animacion;
		TFrameAnimacion* frame = anim->frames[anim->frame_id];

		cpct_drawSolidBox(e->pscreen,0,e->pw,e->ph);

		if ( frame )
		{
			//cpct_drawSolidBox(e->pscreen,0,e->pw,e->ph);
			cpct_drawSpriteMasked(frame->sprite,e->npscreen,frame->width,frame->height);	
		}
		/*else {
			cpct_drawSolidBox(e->pscreen,0,e->pw,e->ph);
		}	*/
	}
	

}

// Dibuja la puntuacion actual
void dibujarPuntuacion() {

	u8 str[6]; // Array para mostrar la puntuacion maxima

	sprintf(str, "%5u", g_Puntuacion);
	cpct_drawStringM1(str,calcularPosicion(67,53),2,0);
	cambiaPuntuacion = 0;

}

/*// Dibuja el fondo de la pantalla
void dibujarFondo(TEntidad* e) {

	cpct_drawSolidBox(e->pscreen,0,1,1);
	cpct_drawSolidBox(e->npscreen,1,1,1);

}*/


// Dibuja todo lo que haya cambiado en la pantalla
void dibujar() {

	u16 cont;
	//u8 str[6]; // Array para mostrar la puntuacion maxima
	u8 a = 60;


	TPersonaje *p = g_Enemigos;
	
   

   /*while (--cont)
   {
   		if (g_ultimoTDibu == 7)
   		{
   			dibujarFondo(d);		
   			d->act = 1;
   		}
   		d++;
   }*/

  /* for (cont = 0; cont < g_ultimoFondo; cont++) {
   		//if (g_ultimoTDibu == 7)
   		//{
   			dibujarFondo(&g_FondoPantalla[cont]);
   		//}
		
	}*/
   

   TEntidad *d = g_Disparos;
   
   cont = g_ultimoDisparo + 1;

   while (--cont) {

 		dibujarDisparos(d,1);
 		//dibujarDisparo(d); 
    	d++;

   }

   d = g_DisparosEnemigos;

   cont = g_ultimoDisparoEnemigo + 1;

   while (--cont) {

   		dibujarDisparos(d,0);
      	//dibujarDisparo(d);
      	d++;

   }


	/*// Dibuja los disparos
	for (cont = 0; cont < g_ultimoDisparo; cont++) {
		dibujarDisparo(&g_Disparos[cont]);
	}*/

	// Dibuja los disparos enemigos
	/*for (cont = 0; cont < g_ultimoDisparoEnemigo; cont++) {
		dibujarDisparoEnemigo(&g_DisparosEnemigos[cont]);
	}*/

	cont = g_ultimoEnemigo + 1;

	while (--cont) {

		//sprintf(str, "%5u", cont);
		//cpct_drawStringM1(str,calcularPosicion(50,30),2,0);

		//sprintf(str, "%5u", p->entidad.tDibu);
		//cpct_drawStringM1(str,calcularPosicion(50,a),2,0);
		a=a+10;

		if (p->entidad.tDibu == g_ultimoTDibu || p->estado == es_movimiento || p->estado == es_muerto || p->estado == es_atrapando)
		{
			dibujarEnemigos(&p->entidad);
			p->entidad.act = 0;
		}
		p++;

	}
	a = 10;

/*
	// Dibuja los enemigos
	for (cont = 0; cont < g_ultimoEnemigo; cont++) {
		dibujarEnemigos(&g_Enemigos[cont].entidad);	
	}
*/

	dibujarNave(&g_Personaje.entidad);

	if ( cambiaPuntuacion )
	{
		dibujarPuntuacion();	
	}

	//sprintf(str, "%5u", g_ultimoTDibu);
	//cpct_drawStringM1(str,calcularPosicion(50,50),2,0);

	if (g_ultimoTDibu == 8)
	{
		g_ultimoTDibu = 1;
	}
	else
	{
		++g_ultimoTDibu;
	}
	

}