#include <cpctelera.h>
#include "music/p.h"

void interruptHandler();

void initMusica() {

	cpct_waitVSYNC();
	cpct_setInterruptHandler ( interruptHandler );

	cpct_akp_musicInit(g_renegremix);
	cpct_akp_SFXInit  (g_renegremix);
}