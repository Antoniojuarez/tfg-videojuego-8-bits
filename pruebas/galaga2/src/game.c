#include <stdio.h>
#include <cpctelera.h>
#include "entities.h"
#include "sprites.h"
#include "constantes.h"


u8 marcador;
u8 jugadores;

u8 menuPrincipal(u16 hi) {

	u8 str[6]; // Array para mostrar la puntuacion maxima

	//cpct_fw2hw(Titulo_palette,4);
  	cpct_setPalette(Titulo_palette,4);

  	cpct_drawStringM1("1UP",calcularPosicion(10,10),3,0);

  	cpct_drawStringM1("00",calcularPosicion(18,18),2,0);

  	cpct_drawStringM1("HI-SCORE",calcularPosicion(26,10),3,0);

  	sprintf(str, "%5u", hi);
  	cpct_drawStringM1(str,calcularPosicion(30,18),2,0);

  	cpct_drawStringM1("2UP",calcularPosicion(46,10),3,0);

  	cpct_drawStringM1("00",calcularPosicion(54,18),2,0);

  	cpct_drawStringM1("1 PLAYER",calcularPosicion(26,120),2,0);

  	cpct_drawStringM1("2 PLAYERS",calcularPosicion(26,136),2,0);

  	cpct_drawCharM1(calcularPosicion(22,120),2,0,62);

  	cpct_drawSprite(g_tile_Titulo,calcularPosicion(20,40),28,56);

	marcador = 1;
	jugadores = 1;

	{
		u8 colores[2] = {HW_GREEN,HW_LIME};

		u8 indiceColor = 0;

		u16 tiempo = 1300;

		// Permite seleccionar el numero de jugadores de la partida
		while(marcador == 1)
		{

			cpct_scanKeyboard_f();

			if (cpct_isKeyPressed(Key_CursorDown) ) {
				cpct_drawCharM1(calcularPosicion(22,120),2,0,32);
				cpct_drawCharM1(calcularPosicion(22,136),2,0,62);
				jugadores = 2;
			}
			else if (cpct_isKeyPressed(Key_CursorUp) ) {
				cpct_drawCharM1(calcularPosicion(22,120),2,0,62);
				cpct_drawCharM1(calcularPosicion(22,136),2,0,32);	
				jugadores = 1;
			}
			else if (cpct_isKeyPressed(Key_Space) ) {
				marcador = 0;
			}

			tiempo--;

			if (tiempo == 0)
			{
				indiceColor = indiceColor+1;

				cpct_setPALColour(1,indiceColor);

				if (indiceColor > 1) {
					indiceColor = 0;
				}

				tiempo = 1300;

			}

		}
	}

	return jugadores;

}

void espera(u8 vsyncs) {
   do {
      cpct_waitVSYNC();
      __asm 
         halt
         halt
      __endasm;
   } while (--vsyncs);
}

void inicializarEscenario(u16 hi, u8 pantalla, u8 vidas, u16 puntos) {

	u8 str[6]; // Array para mostrar la puntuacion maxima
	u32 salto = 60000;	// Controla el cambio de mensajes

	u8 nivIni = 65; // Posicion de inicio en el dibujado de los simbolos
	u8 nivDes = 2; // Desplazamiento en el dibujado de los simbolos de los niveles
	u8 i = 0;

	// Configura la paleta de colores para el juego
    //cpct_fw2hw(Juego_palette,4);
    cpct_setPalette(Juego_palette,4);

	cpct_clearScreen(0);

	cpct_drawStringM1("NIVEL",calcularPosicion(26,100),1,0);

	sprintf(str, "%5u", pantalla);
	cpct_drawStringM1(str,calcularPosicion(38,100),1,0);

	while(salto != 0)
	{
		salto--;
	}

	cpct_clearScreen(0);

	salto = 10000;

	cpct_drawStringM1("START",calcularPosicion(26,100),2,0);


   espera(100);
   //while(--salto);
/*
	while(salto != 0)
	{
		salto--;
	}
*/
	cpct_clearScreen(0);

	cpct_drawStringM1("HIGH",calcularPosicion(65,5),3,0);

	cpct_drawStringM1("SCORE",calcularPosicion(67,13),3,0);

	sprintf(str, "%5u", hi);
  	cpct_drawStringM1(str,calcularPosicion(67,21),2,0);

  	cpct_drawStringM1("1UP",calcularPosicion(65,45),3,0);

  	sprintf(str, "%5u", puntos);
	cpct_drawStringM1(str,calcularPosicion(65,53),2,0);

	// Dibujar las vidas
	if (vidas == 2)
	{
		cpct_drawSpriteMasked(g_tile_Nave,calcularPosicion(65,103),4,16);
		cpct_drawSpriteMasked(g_tile_Nave,calcularPosicion(69,103),4,16);	
	}
	else if (vidas == 1)
	{
		cpct_drawSpriteMasked(g_tile_Nave,calcularPosicion(65,103),4,16);
	}

	if (pantalla == 10)
	{
		cpct_drawSpriteMasked(g_tile_Nivel10,calcularPosicion(65,120),4,16);
	}
	else if (pantalla >= 5)
	{
		cpct_drawSpriteMasked(g_tile_Nivel5,calcularPosicion(65,120),2,16);

		for (i = pantalla;i > 5;--i)
		{
			nivIni = nivIni+nivDes;
			cpct_drawSpriteMasked(g_tile_Nivel1,calcularPosicion(nivIni,120),2,16);
		}
	}
	else
	{
		for (i = pantalla; i > 0; --i)
		{
			cpct_drawSpriteMasked(g_tile_Nivel1,calcularPosicion(nivIni,120),2,16);
			nivIni = nivIni+nivDes;
		}
	}
	

	cpct_drawSpriteMasked(g_tile_Nave,calcularPosicion(36,170),4,16);

/*
	cpct_drawStringM1("B",calcularPosicion(62,10),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,18),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,26),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,34),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,42),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,50),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,58),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,66),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,74),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,82),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,90),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,98),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,106),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,114),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,122),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,130),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,138),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,146),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,154),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,162),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,170),3,0);
	cpct_drawStringM1("B",calcularPosicion(62,178),3,0);

	cpct_drawStringM1("B",calcularPosicion(10,10),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,18),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,26),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,34),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,42),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,50),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,58),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,66),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,74),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,82),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,90),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,98),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,106),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,114),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,122),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,130),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,138),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,146),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,154),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,162),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,170),3,0);
	cpct_drawStringM1("B",calcularPosicion(10,178),3,0);

	cpct_drawStringM1("BBBBBBBBBBBBBBBBBBBBBBBBBBB",calcularPosicion(10,2),3,0);
	cpct_drawStringM1("BBBBBBBBBBBBBBBBBBBBBBBBBBB",calcularPosicion(10,186),3,0);*/


}

// Comprueba el movimiento del jugador, para preparar el estado de la nave
void actualizarEstado(TPersonaje* nave) {

	// Escaneamos la entrada de teclado
	cpct_scanKeyboard_f();

	// Comprobamos las posibles teclas a pulsar

	// Tecla = Cursor derecha, Accion = Moverse a la derecha
	if ( cpct_isKeyPressed(Key_CursorRight) )
	{
		realizarAccion(nave, es_movimiento, s_derecha);
	}

	// Tecla = Cursor izquierda, Accion = Moverse a la izquierda
	else if ( cpct_isKeyPressed(Key_CursorLeft) )
	{
		realizarAccion(nave, es_movimiento, s_izquierda);
	}

	// Tecla = Cursor arriba, Accion = Disparar
	else if ( cpct_isKeyPressed(Key_CursorUp) )
	{
		realizarAccion(nave, es_disparando, s_nada);
	}

	// Tecla = Ninguna pulsada, Accion = Dejar estatico
	else {
		realizarAccion(nave, es_parado, s_nada);
	}
}

void wait4Key(cpct_keyID key) {
   do
      cpct_scanKeyboard_f();
   while( ! cpct_isKeyPressed(key) );
   do
      cpct_scanKeyboard_f();
   while( cpct_isKeyPressed(key) );
}

// Muestra los resultados al finalizar la partida
void mostrarResultados()
{

	u8 str[6]; // Array para mostrar la puntuacion maxima
	u8 str2[8];
	u16 total1;
	u16 bajas1;
	u16 bajas2;
	u16 total2;

	u8 i;


	cpct_clearScreen(0);

	cpct_drawStringM1("PUNTUACIONES",calcularPosicion(25,50),3,0);

	cpct_drawSpriteMasked(g_tile_Enemigo1_0,calcularPosicion(20,80),4,12);

	cpct_drawSpriteMasked(g_tile_Enemigo2_0,calcularPosicion(20,100),4,12);

	cpct_drawStringM1("x",calcularPosicion(25,80),3,0);

	cpct_drawStringM1("x",calcularPosicion(25,100),3,0);

	bajas1 = getBajas1();

	sprintf(str, "%5u", bajas1);

	cpct_drawStringM1(str,calcularPosicion(27,80),3,0);

	bajas2 = getBajas2();

	sprintf(str, "%5u", bajas2);

	cpct_drawStringM1(str,calcularPosicion(27,100),3,0);

	cpct_drawStringM1("=",calcularPosicion(40,80),3,0);

	cpct_drawStringM1("=",calcularPosicion(40,100),3,0);
 
	total1 = bajas1 * 100;

	sprintf(str2, "%5u", total1);

	cpct_drawStringM1(str2,calcularPosicion(42,80),3,0);

	total2 = bajas2 * 100;

	sprintf(str2, "%5u", total2);

	cpct_drawStringM1(str2,calcularPosicion(42,100),3,0);

	cpct_drawStringM1("PULSA ESPACIO PARA CONTINUAR",calcularPosicion(10,140),3,0);

	// Wait for SPACE being pressed before continuing
   	wait4Key(Key_Space);
}


// Permite jugar una partida completa
u16 game(u8 jugadores,u16 hi) {

	u8 vivo = 1;			// Indica si el personaje principal sigue vivo
	u8 vidas = 2;			// Numero de vidas del personaje
	u8 pantalla = 1;		// Numero de la pantalla
	u16 puntos = 0;			// Puntuacion actual del jugador
	TPersonaje* nave;	    // Puntero al personaje principal


	inicializarEscenario(hi,pantalla,vidas,puntos); // Deja preparado el entorno del juego
	inicializarEntidades(pantalla); // Inicializa los elementos de la pantalla
	nave = getPersonaje();

	while(vivo){

		actualizarEstado(nave); // Actualiza el movimiento del jugador
		actualizarPantalla(); // Actualiza los elementos de la pantalla
		vivo = actualizarNave(nave); // Actualiza el estado de la nave
		cpct_waitVSYNC();
		dibujar();

		// Si han destruido la nave y aun quedan vidas
		if (vivo == 0 && vidas > 0)
		{
			vidas--;
			vivo = 1;
			puntos = getPuntuacion();
			inicializarEscenario(hi,pantalla,vidas,puntos);
			actualizarEntidades(); // Reinicia la partida
			nave = getPersonaje();
		}

		// Si hemos superado una pantalla
		else if (vivo == 2)
		{
			vivo = 1;
			pantalla++;
			puntos = getPuntuacion();
			inicializarEscenario(hi,pantalla,vidas,puntos);
			inicializarEntidades(pantalla);
		}

	}

	mostrarResultados();

	return getPuntuacion();

}