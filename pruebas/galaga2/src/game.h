
#ifndef _GAME_H_
#define _GAME_H_


#include <types.h>

extern struct Personaje;
typedef struct Personaje TPersonaje;

void inicializarEscenario(u16 hi);
void actualizarEstado(TPersonaje* nave);
u16 game(u8 jugadores, u16 hi);
u8 menuPrincipal(u16 hi);

#endif